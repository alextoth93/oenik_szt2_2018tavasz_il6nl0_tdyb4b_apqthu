var searchData=
[
  ['data',['Data',['../namespace_data.html',1,'']]],
  ['deletebeszallito',['DeleteBeszallito',['../class_b_l_1_1_beszallito_logic.html#a1e5cb581e909e419614eaf574ce5bccb',1,'BL::BeszallitoLogic']]],
  ['deleterendeles',['DeleteRendeles',['../class_b_l_1_1_rendeles_logic.html#af5fc41b8de860c00761a2ce0581d2ec6',1,'BL::RendelesLogic']]],
  ['deletetermek',['DeleteTermek',['../class_b_l_1_1_termek_logic.html#aef86addd26a47cb7d1f82fb8e7311a6d',1,'BL::TermekLogic']]],
  ['deletetetel',['DeleteTetel',['../class_b_l_1_1_tetel_logic.html#ad2e2eef233b0386ca786a5bc38bd07c5',1,'BL::TetelLogic']]],
  ['dg',['Dg',['../class_l1dl___w_p_f_1_1_view_model_1_1_beszallito_hozzaadas_modositas_v_m.html#a657a5e381578877592953a4bd52ce59b',1,'L1dl_WPF.ViewModel.BeszallitoHozzaadasModositasVM.Dg()'],['../class_l1dl___w_p_f_1_1_view_model_1_1_home_v_m.html#a8c5e7877e7bafbff12d2558ca063a7f5',1,'L1dl_WPF.ViewModel.HomeVM.Dg()'],['../class_l1dl___w_p_f_1_1_view_model_1_1_rendeles_v_m.html#a105320469cef7649eaad2ba29e84ab6f',1,'L1dl_WPF.ViewModel.RendelesVM.Dg()'],['../class_l1dl___w_p_f_1_1_view_model_1_1_termek_hozzaadas_modositas_v_m.html#a722866c7275385b053711b889a85e128',1,'L1dl_WPF.ViewModel.TermekHozzaadasModositasVM.Dg()']]]
];
