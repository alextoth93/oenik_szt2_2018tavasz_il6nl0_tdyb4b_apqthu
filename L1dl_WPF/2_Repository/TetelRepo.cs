﻿// <copyright file="TetelRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

   public class TetelRepo : GenericRepository<Tetel>, ITetelRepo
    {
        public async override Task<Tetel> GetById(int id)
        {
            return await Task.Factory.StartNew(() => { return (from q in this.Context.Tetel where q.TetelId == id select q).SingleOrDefault(); });
        }

        public async Task ModifyTetel(int tetelId, int? rendelesId = null, int? cikkszam = null, int? mennyiseg = null)
        {
            var old = await this.GetById(tetelId);
            if (old == null)
            {
                throw new ArgumentException("Nincs ilyen tétel");
            }

            if (rendelesId != null)
            {
                old.RendelesId = rendelesId.Value;
            }

            if (cikkszam != null)
            {
                old.Cikkszam = cikkszam.Value;
            }

            if (mennyiseg != null)
            {
                old.Mennyiseg = mennyiseg.Value;
            }

            await this.Context.SaveChangesAsync();
        }
    }
}
