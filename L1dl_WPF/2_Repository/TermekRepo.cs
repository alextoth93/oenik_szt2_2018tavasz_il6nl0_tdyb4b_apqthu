﻿// <copyright file="TermekRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

   public class TermekRepo : GenericRepository<Termek>, ITermekRepo
    {
        public async override Task<Termek> GetById(int cikkszam)
        {
            return await Task.Factory.StartNew(() => { return (from q in this.Context.Termek where q.CikkSzam == cikkszam select q).SingleOrDefault(); });
        }

        public async Task<bool> ErvenyesE(int cikkszam)
        {
            return await Task.Factory.StartNew(() => { return (from q in this.Context.Termek where q.CikkSzam == cikkszam select q).SingleOrDefault() == null; });
        }

        public async Task ModifyTermek(int cikkszam, int? vonalkod = null, string nev = null, string leiras = null, int? ar = null, int? mennyiseg = null, string toroltE = null)
        {
            var old = await this.GetById(cikkszam);
            if (old == null)
            {
                throw new ArgumentException("Nincs ilyen termék");
            }

            if (nev != null)
            {
                old.TermekNev = nev;
            }

            if (leiras != null)
            {
                old.TermekLeiras = leiras;
            }

            if (ar != null)
            {
                old.Ar = ar.Value;
            }

            if (mennyiseg != null)
            {
                old.Mennyiseg = mennyiseg.Value;
            }

            if (vonalkod != null)
            {
                old.VonalKod = vonalkod.Value;
            }

            if (toroltE != null)
            {
                old.ToroltE = toroltE;
            }

            await this.Context.SaveChangesAsync();
        }
    }
}
