﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Tests
{
    [TestClass()]
    public class BeszallitoLogicTests
    {
        [TestMethod()]
        public void AddBeszallitoTest()
        {
            var termekek = new TermekLogic().GetTermek().Result.Take(2).ToList();
            BeszallitoLogic logic = new BeszallitoLogic();
            string nev = "Teszty";
            var result = logic.AddBeszallito(nev, termekek).Result;
            var d = logic.GetBeszallito().Result;
        }

        [TestMethod()]
        public void DeleteBeszallitoTest()
        {
            BeszallitoLogic logic = new BeszallitoLogic();
            var result = logic.DeleteBeszallito(1).Result;
            Assert.AreEqual(result, "Sikeres törlés");
        }

        [TestMethod()]
        public void GetBeszallitoTest()
        {
            BeszallitoLogic logic = new BeszallitoLogic();
            var result = logic.GetBeszallito();
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void GetBeszallitoTest_single()
        {
            BeszallitoLogic logic = new BeszallitoLogic();
            var result = logic.GetBeszallito(1);
            Assert.IsNotNull(result);
        }


        [TestMethod()]
        public void ModifyBeszallitoTest()
        {
            BeszallitoLogic logic = new BeszallitoLogic();
            var result = logic.ModifyBeszallito(1, "teszt").Result;
            Assert.AreEqual(result, "Sikeres módosítás");
        }

        [TestMethod()]
        public void ModifyBeszallitoTest_listaval()
        {
            BeszallitoLogic logic = new BeszallitoLogic();
            var list = new TermekLogic().GetTermek().Result;
            var result = logic.ModifyBeszallito(1, "teszt", list).Result;
            Assert.AreEqual(result, "Sikeres módosítás");
            
        }
    }
}