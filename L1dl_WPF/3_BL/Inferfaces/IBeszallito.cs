﻿// <copyright file="IBeszallito.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IBeszallito
    {
        Task<string> AddBeszallito(string nev, List<Data.Termek> szallitottTermekek);

        Task<string> ModifyBeszallito(int beszallitoID, string nev = null, List<Data.Termek> szallitottTermekek = null);

        Task<string> DeleteBeszallito(int beszallitoID);

        Task<List<Data.Beszallito>> GetBeszallito(int? beszallitoID = null);
    }
}
