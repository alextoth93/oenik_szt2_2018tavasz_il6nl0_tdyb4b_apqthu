namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("BeszallitoTermekConnector")]
    public partial class BeszallitoTermekConnector
    {
        public int BeszallitoTermekConnectorId { get; set; }

        public int BeszallitoId { get; set; }

        public int Cikkszam { get; set; }

        public virtual Beszallito Beszallito { get; set; }

        public virtual Termek Termek { get; set; }
        public override string ToString()
        {
            return "Cikkszám: " + Cikkszam + " " + "Terméknév: " + Termek.TermekNev;
        }
    }
}
