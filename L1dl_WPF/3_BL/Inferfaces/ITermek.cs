﻿// <copyright file="ITermek.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Termékekért felelős interface
    /// </summary>
    public interface ITermek
    {
        Task<string> AddTermek(int cikkszam, int vonalkod, string termekNev, string termekLeiras, int ar, int mennyiseg);

        Task<string> ModifyTermek(int termekID, int? vonalkod = null, string termekNev = null, string termekLeiras = null, int? ar = null, int? mennyiseg = null, List<int> beszallitoId = null);

        Task<string> DeleteTermek(int termekID);

        Task<List<Data.Termek>> GetTermek(int? termekID = null);

        Task<bool> ErvenyesCikkszamE(int cikkszam);
    }
}
