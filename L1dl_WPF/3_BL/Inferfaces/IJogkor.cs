﻿// <copyright file="IJogkor.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Jogkörért felelős interface
    /// </summary>
    public interface IJogkor
    {
        Task<IEnumerable<Data.Jogkor>> GetJogkor(int? jogkorId = null);
    }
}
