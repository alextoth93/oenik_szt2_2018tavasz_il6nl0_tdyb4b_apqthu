﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Tests
{
    [TestClass()]
    public class FelhasznaloLogicTests
    {
        [TestMethod()]
        public void GenerateHashTest()
        {
            FelhasznaloLogic logic = new FelhasznaloLogic();
            string s = "1234";
            string s2 = "03AC674216F3E15C761EE1A5E255F067953623C8B388B4459E13F978D7C846F4";
            var result = logic.GenerateHash(s);
            Assert.AreEqual(result, s2);
        }

        [TestMethod()]
        public void GenerateHashTest_invalid()
        {
            FelhasznaloLogic logic = new FelhasznaloLogic();
            string s = "alma";
            var result = logic.GenerateHash(s + "bla");
            Assert.AreNotEqual(result, s.GetHashCode().ToString());
        }

        [TestMethod()]
        public void LoginTest()
        {
            FelhasznaloLogic logic = new FelhasznaloLogic();
            var name = "Pum Pál";
            var pass = "1234";
            var result = logic.Login(name, pass)?.Result;
            Assert.IsNotNull(result);
        }


  

        [TestMethod()]
        public void GetFelhasznaloTest()
        {
            FelhasznaloLogic logic = new FelhasznaloLogic();
            var result = logic.GetFelhasznalo(1).Result;
            Assert.IsNotNull(result);
        }

 

        [TestMethod()]
        public void ModifyFelhasznaloTest()
        {
            FelhasznaloLogic logic = new FelhasznaloLogic();
            var result = logic.ModifyFelhasznalo(1, "tesztbéla").Result;
            Assert.AreEqual(result, "Sikeres módosítás");
        }

        [TestMethod()]
        public void ModifyFelhasznaloTest_invalid()
        {
            FelhasznaloLogic logic = new FelhasznaloLogic();
            //  var result = logic.ModifyFelhasznalo(1, "tesztbéla").Result;
            //    Assert.AreEqual(result, "Sikeres módosítás");
        }

        [TestMethod()]
        public void ChangePasswordTest()
        {
            FelhasznaloLogic logic = new FelhasznaloLogic();
            string oldP = "1234";
            string newP = "4567";
            var result = logic.ChangePassword(1, oldP, newP).Result;
            Assert.AreEqual(result, "Sikeres jelszómódosítás!");
        }

    }
}