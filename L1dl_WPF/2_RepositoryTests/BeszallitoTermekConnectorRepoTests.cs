﻿using Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Tests
{
    [TestClass()]
    public class BeszallitoTermekConnectorRepoTests
    {
        [TestMethod()]
        public void AddRangeTest()
        {
            BeszallitoTermekConnectorRepo repo = new BeszallitoTermekConnectorRepo();
            List<Termek> list = new TermekRepo().GetAll().Result.ToList();
            List<BeszallitoTermekConnector> r = new List<BeszallitoTermekConnector>();
            foreach (var item in list)
            {
                r.Add(new BeszallitoTermekConnector() { BeszallitoId = 1, Cikkszam = item.CikkSzam });
            }
            repo.AddRange(r).Wait();
            var d = repo.GetAll().Result.ToList();
        }
    }
}