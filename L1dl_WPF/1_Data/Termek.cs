namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Termek")]
    public partial class Termek
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Termek()
        {
            BeszallitoTermekConnector = new HashSet<BeszallitoTermekConnector>();
            Tetel = new HashSet<Tetel>();
        }

        [Key]
        public int CikkSzam { get; set; }

        public int VonalKod { get; set; }

        [Required]
        [StringLength(255)]
        public string TermekNev { get; set; }

        [StringLength(255)]
        public string TermekLeiras { get; set; }

        public int Ar { get; set; }

        public int Mennyiseg { get; set; }

        [Required]
        [StringLength(1)]
        public string ToroltE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BeszallitoTermekConnector> BeszallitoTermekConnector { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tetel> Tetel { get; set; }


        [NotMapped]
        public List<Beszallito> Beszallitok { get; set; }
    }
}
