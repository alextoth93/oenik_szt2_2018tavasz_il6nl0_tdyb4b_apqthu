﻿// <copyright file="BeszallitoTermekConnectorRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

    public class BeszallitoTermekConnectorRepo : GenericRepository<BeszallitoTermekConnector>, IBeszallitoTermekConnectorRepo
    {
        public override Task<BeszallitoTermekConnector> GetById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task AddRange(IEnumerable<BeszallitoTermekConnector> input)
        {
            this.Context.BeszallitoTermekConnector.AddRange(input);
            await this.Context.SaveChangesAsync();
        }

        public async Task RemoveRange(IEnumerable<BeszallitoTermekConnector> input)
        {
            await Task.Factory.StartNew(() =>
            {
                foreach (var item in input)
                {
                    var v = this.Context.BeszallitoTermekConnector.SingleOrDefault(x => x.BeszallitoTermekConnectorId == item.BeszallitoTermekConnectorId);
                    this.Context.BeszallitoTermekConnector.Remove(v);
                }
            });
        }
    }
}
