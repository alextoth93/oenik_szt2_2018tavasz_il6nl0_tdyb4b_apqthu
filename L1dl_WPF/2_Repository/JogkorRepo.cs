﻿// <copyright file="JogkorRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

   public class JogkorRepo : GenericRepository<Jogkor>, IJogkorRepo
    {
        public async override Task<Jogkor> GetById(int id)
        {
            return await Task.Factory.StartNew(() =>
            {
                return (from q in this.Context.Jogkor where q.JogkorId == id select q).SingleOrDefault();
            });
        }
    }
}
