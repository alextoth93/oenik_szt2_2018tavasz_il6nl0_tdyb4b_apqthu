﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Tests
{
    [TestClass()]
    public class JogkorRepoTests
    {
        [TestMethod()]
        public void GetByIdTest()
        {
            JogkorRepo r = new JogkorRepo();
            var result = r.GetById(1);
            Assert.IsNotNull(result);
        }

        [TestMethod()]
        public void GetByIdTest_invalid()
        {
            TermekRepo r = new TermekRepo();
            //Data.Termek t = new Data.Termek() { CikkSzam = 10000, Ar = 10, Mennyiseg = 0, TermekNev = "SANYI", VonalKod = 123456 };
            r.ModifyTermek(10000, nev:"asdsadasdasdasdas").Wait();
            var asdsad = r.GetAll().Result.ToList();
            JogkorRepo r2 = new JogkorRepo();
  //          var result = r.GetById(-10);
  //          Assert.IsNull(result);
        }
    }
}