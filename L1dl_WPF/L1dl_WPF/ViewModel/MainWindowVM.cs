﻿// <copyright file="MainWindowVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF.ViewModel
{
    using System.ComponentModel;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using BL;
    using Data;

    /// <summary>
    /// MainWindow View modelljéért felelős osztály
    /// </summary>
    public class MainWindowVM : INotifyPropertyChanged
    {
        private IFelhasznalo logic;
        private string felhasznaloID;
        private string felhasznaloNev;
        private string felhasznaloJelszo;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowVM"/> class.
        /// Paraméter nélküli konstruktora
        /// </summary>
        public MainWindowVM()
        {
            this.logic = new FelhasznaloLogic();
        }

        /// <summary>
        /// Property változásáért felelős eventhandler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Submit command ICommand vissszatérési értékkel
        /// </summary>
        public ICommand SubmitCommand { get; set; }

        /// <summary>
        /// Felhasználó id
        /// </summary>
        public string FelhasznaloID
        {
            get
            {
                return this.felhasznaloID;
            }

            set
            {
                this.felhasznaloID = value;
                this.OnPropertyChanged("FelhasznaloID");
            }
        }

        /// <summary>
        /// Felhasználó név
        /// </summary>
        public string FelhasznaloNev
        {
            get
            {
                return this.felhasznaloNev;
            }

            set
            {
                this.felhasznaloNev = value;
                this.OnPropertyChanged("FelhasznaloNev");
            }
        }

        /// <summary>
        /// Felhasználó jelzó
        /// </summary>
        public string FelhasznaloJelszo
        {
            get
            {
                return this.felhasznaloJelszo;
            }

            set
            {
                this.felhasznaloJelszo = value;
                this.OnPropertyChanged("FelhasznaloJelszo");
            }
        }

        /// <summary>
        /// Bejelentkezés
        /// </summary>
        /// <param name="text">szöveg</param>
        /// <param name="password">jelszó</param>
        /// <returns>task</returns>
        public async Task<Felhasznalo> Login(string text, string password)
        {
            return await this.logic.Login(text, password);
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
