﻿// <copyright file="IFelhasznalo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Felhasználóért felelős interface
    /// </summary>
    public interface IFelhasznalo
    {
        // string AddFelhasznalo(string Felhasznalonev, string Jelszo, int JogkorID);
        Task<string> ModifyFelhasznalo(int felhasznaloID, string felhasznalonev = null, int? jogkorID = null, int? utolsoAblak = null);

        // string DeleteUser(int FelhasznaloID);
        Task<Data.Felhasznalo> GetFelhasznalo(int felhasznaloID);

        Task<Data.Felhasznalo> Login(string felhasznalonev, string jelszo);

        Task<string> ChangePassword(int felhasznaloID, string oldJelszo, string newJelszo);
    }
}
