namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Tetel")]
    public partial class Tetel
    {
        public int TetelId { get; set; }

        public int RendelesId { get; set; }

        public int Cikkszam { get; set; }

        public int Mennyiseg { get; set; }

        public virtual Rendeles Rendeles { get; set; }

        public virtual Termek Termek { get; set; }
    }
}
