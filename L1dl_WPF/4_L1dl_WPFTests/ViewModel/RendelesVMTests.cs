﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using L1dl_WPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1dl_WPF.ViewModel.Tests
{
    [TestClass()]
    public class RendelesVMTests
    {
        [TestMethod()]
        public void TetelLetrehozTest()
        {
            RendelesVM vm = new RendelesVM(null, "uj", 1, null, null);
            var old = vm.TetelLista.Count();
            vm.TetelLetrehoz(10000, "dezso", 100);
            var newasd = vm.TetelLista.Count();
            Assert.AreNotEqual(old, newasd);

        }
    }
}