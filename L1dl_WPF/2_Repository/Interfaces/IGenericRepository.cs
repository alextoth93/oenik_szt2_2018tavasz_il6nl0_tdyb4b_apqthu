﻿// <copyright file="IGenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;

    public interface IGenericRepository<TEntity>
        where TEntity : class
    {
        Task<TEntity> GetById(int id);

        Task Delete(int id);

        Task Delete(TEntity oldentity);

        void Dispose();

        Task<IQueryable<TEntity>> Get(Expression<Func<TEntity, bool>> condition);

        Task<IQueryable<TEntity>> GetAll();

        Task Insert(TEntity newentity);
    }
}
