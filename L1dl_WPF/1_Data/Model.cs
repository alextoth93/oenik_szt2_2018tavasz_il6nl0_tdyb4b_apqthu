namespace Data
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model : DbContext
    {
        public Model()
            : base("name=EFModel")
        {
        }

        public virtual DbSet<Beszallito> Beszallito { get; set; }
        public virtual DbSet<BeszallitoTermekConnector> BeszallitoTermekConnector { get; set; }
        public virtual DbSet<Felhasznalo> Felhasznalo { get; set; }
        public virtual DbSet<Jogkor> Jogkor { get; set; }
        public virtual DbSet<Rendeles> Rendeles { get; set; }
        public virtual DbSet<Termek> Termek { get; set; }
        public virtual DbSet<Tetel> Tetel { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Beszallito>()
                .Property(e => e.BeszallitoNev)
                .IsUnicode(false);

            modelBuilder.Entity<Beszallito>()
                .Property(e => e.ToroltE)
                .IsUnicode(false);

            modelBuilder.Entity<Beszallito>()
                .HasMany(e => e.BeszallitoTermekConnector)
                .WithRequired(e => e.Beszallito)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Beszallito>()
                .HasMany(e => e.Rendeles)
                .WithRequired(e => e.Beszallito)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Felhasznalo>()
                .Property(e => e.FelhasznaloNev)
                .IsUnicode(false);

            modelBuilder.Entity<Felhasznalo>()
                .Property(e => e.Jelszo)
                .IsUnicode(false);

            modelBuilder.Entity<Felhasznalo>()
                .Property(e => e.ToroltE)
                .IsUnicode(false);

            modelBuilder.Entity<Felhasznalo>()
                .HasMany(e => e.Rendeles)
                .WithRequired(e => e.Felhasznalo)
                .HasForeignKey(e => e.FelelosSzemelyId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Jogkor>()
                .Property(e => e.JogkorNev)
                .IsUnicode(false);

            modelBuilder.Entity<Jogkor>()
                .HasMany(e => e.Felhasznalo)
                .WithRequired(e => e.Jogkor)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Rendeles>()
                .HasMany(e => e.Tetel)
                .WithRequired(e => e.Rendeles)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Termek>()
                .Property(e => e.TermekNev)
                .IsUnicode(false);

            modelBuilder.Entity<Termek>()
                .Property(e => e.TermekLeiras)
                .IsUnicode(false);

            modelBuilder.Entity<Termek>()
                .Property(e => e.ToroltE)
                .IsUnicode(false);

            modelBuilder.Entity<Termek>()
                .HasMany(e => e.BeszallitoTermekConnector)
                .WithRequired(e => e.Termek)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Termek>()
                .HasMany(e => e.Tetel)
                .WithRequired(e => e.Termek)
                .WillCascadeOnDelete(false);
        }
    }
}
