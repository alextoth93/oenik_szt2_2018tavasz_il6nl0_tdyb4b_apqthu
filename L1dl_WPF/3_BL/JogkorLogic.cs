﻿// <copyright file="JogkorLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Repository;

    /// <summary>
    /// Jogkör logikát leíró osztály
    /// </summary>
    public class JogkorLogic : ParentLogic, IJogkor
    {
        private IJogkorRepo repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="JogkorLogic"/> class.
        /// paraméter nélküli konstruktor
        /// </summary>
        public JogkorLogic()
        {
            this.repo = new JogkorRepo();
        }

        /// <summary>
        /// Megkapjuk a Jogkort
        /// </summary>
        /// <param name="jogkorId">várunk egy számot</param>
        /// <returns>egy Jogkort kapunk vissza</returns>
        public async Task<IEnumerable<Jogkor>> GetJogkor(int? jogkorId = null)
        {
            if (jogkorId == null)
            {
                return await this.repo.GetAll();
            }

            var j = await this.repo.GetById(jogkorId.Value);
            if (j == null)
            {
                throw new ArgumentException("Érvénytelen azonosító!");
            }

            return new Jogkor[] { j };
        }
    }
}
