﻿// <copyright file="RendelesLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Repository;

    /// <summary>
    /// Rendelés logikáért felelős osztály
    /// </summary>
    public class RendelesLogic : ParentLogic, IRendeles
    {
        private IRendelesRepo repo;
        private ITetelRepo tetelRepo;

        /// <summary>
        /// paraméter nélküli konstruktora
        /// </summary>
        public RendelesLogic()
        {
            this.repo = new RendelesRepo();
            this.tetelRepo = new TetelRepo();
        }

        /// <summary>
        /// Rendelés hozzáadása
        /// </summary>
        /// <param name="beszallitoID">beszállító idj-e</param>
        /// <param name="datum">dátum</param>
        /// <param name="felelosSzemely">a felelős személy</param>
        /// <param name="rendeltTetelek">tételek</param>
        /// <returns>válasz</returns>
        public async Task<string> AddRendeles(int beszallitoID, DateTime datum, int felelosSzemely, List<Tetel> rendeltTetelek)
        {
            if ((await new BeszallitoRepo().GetById(beszallitoID)) == null)
            {
                throw new ArgumentException("Nincs ilyen beszállító");
            }

            if ((await new FelhasznaloRepo().GetById(felelosSzemely)) == null)
            {
                throw new ArgumentException("Nincs ilyen felhasználó");
            }

            try
            {
                Rendeles r = new Rendeles() { Datum = datum, FelelosSzemelyId = felelosSzemely, BeszallitoId = beszallitoID };

       // await this.repo.Insert(r);
                foreach (var item in rendeltTetelek)
                {
                    item.Rendeles = r;
                    await this.tetelRepo.Insert(item);
                }

                return "Sikeresen hozzáadva";
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }

        /// <summary>
        /// Rendelés törlése
        /// </summary>
        /// <param name="rendelesID">Rendelés id-je</param>
        /// <returns>válasz</returns>
        public async Task<string> DeleteRendeles(int rendelesID)
        {
            try
            {
                var v = this.tetelRepo.GetAll().Result.Where(x => x.RendelesId == rendelesID).ToList();
                foreach (var item in v)
                {
                    await this.tetelRepo.Delete(item.TetelId);
                }

                await this.repo.Delete(rendelesID);
                return "Sikeresen törölve";
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }

        /// <summary>
        /// Rendelés megkapása
        /// </summary>
        /// <param name="rendelesID">Rendelés Idja</param>
        /// <returns>Megkapjuk a megfelelő rendelést/eket</returns>
        public async Task<List<Rendeles>> GetRendeles(int? rendelesID = null)
        {
            try
            {
                if (rendelesID == null)
                {
                    return (await this.repo.GetAll()).ToList();
                }

                return new List<Rendeles>() { await this.repo.GetById(rendelesID.Value) };
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }

        /// <summary>
        /// Rendelés módosítása
        /// </summary>
        /// <param name="rendelesID">Rendeles ID-ja</param>
        /// <param name="beszallitoID">Beszallito Id-ja</param>
        /// <param name="datum">Dátum</param>
        /// <param name="felelosSzemely">a felelős személy</param>
        /// <param name="rendeltTetelek">Rendelt tételek</param>
        /// <returns>Visszatérünk egy válasszal (string)</returns>
        public async Task<string> ModifyRendeles(int rendelesID, int? beszallitoID = null, DateTime? datum = null, int? felelosSzemely = null, List<Tetel> rendeltTetelek = null)
        {
            if (beszallitoID != null && (await new BeszallitoRepo().GetById(beszallitoID.Value)) == null)
            {
                throw new ArgumentException("Nincs ilyen beszállító");
            }

            if (felelosSzemely != null && (await new FelhasznaloRepo().GetById(felelosSzemely.Value)) == null)
            {
                throw new ArgumentException("Nincs ilyen felhasználó");
            }

            try
            {
                await this.repo.ModifyRendeles(rendelesID, beszallitoID, datum, felelosSzemely);
                if (rendeltTetelek != null)
                {
                    var old = (await this.repo.GetById(rendelesID)).Tetel.ToList();
                    foreach (var item in old)
                    {
                        await this.tetelRepo.Delete(item.RendelesId);
                    }

                    foreach (var item in rendeltTetelek)
                    {
                        item.RendelesId = rendelesID;
                        await this.tetelRepo.Insert(item);
                    }
                }

                return "Sikeresen módosítva";
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }
    }
}
