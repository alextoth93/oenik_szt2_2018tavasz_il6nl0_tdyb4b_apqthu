namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Jogkor")]
    public partial class Jogkor
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Jogkor()
        {
            Felhasznalo = new HashSet<Felhasznalo>();
        }

        public int JogkorId { get; set; }

        [Required]
        [StringLength(255)]
        public string JogkorNev { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Felhasznalo> Felhasznalo { get; set; }
    }
}
