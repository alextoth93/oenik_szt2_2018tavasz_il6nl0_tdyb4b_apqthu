namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Beszallito")]
    public partial class Beszallito
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Beszallito()
        {
            BeszallitoTermekConnector = new HashSet<BeszallitoTermekConnector>();
            Rendeles = new HashSet<Rendeles>();
        }

        public int BeszallitoId { get; set; }

        [Required]
        [StringLength(255)]
        public string BeszallitoNev { get; set; }

        [Required]
        [StringLength(1)]
        public string ToroltE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BeszallitoTermekConnector> BeszallitoTermekConnector { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rendeles> Rendeles { get; set; }

        [NotMapped]
        public List<Termek> Termekek
        {
            get
            {
                List<Termek> r = new List<Termek>();
                foreach (var item in BeszallitoTermekConnector)
                {
                    r.Add(item.Termek);
                }
                return r;
            }
        }
        
    }
}
