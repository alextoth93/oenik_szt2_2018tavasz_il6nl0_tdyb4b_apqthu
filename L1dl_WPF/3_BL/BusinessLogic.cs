﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

/*
namespace BL
{
    public class BusinessLogic
    {
    }

    public class FelhasznaloLogic : ParentLogic, IFelhasznalo 
    {
        public string AddFelhasznalo(string Felhasznalonev, string Jelszo, int JogkorID)
        {
            return null;
        }

        public string ChangePassword(int FelhasznaloID, string OldJelszo, string NewJelszo)
        {
            return null;
        }

        public string DeleteUser(int FelhasznaloID)
        {
            f.Delete(FelhasznaloID);
            return null;
        }

        public Felhasznalo GetFelhasznalo(int FelhasznaloID)
        {
            return f.GetById(FelhasznaloID);
        }

        public Felhasznalo Login(string Felhasznalonev, string Jelszo)
        {
            //return f.GetById(int.Parse(Felhasznalonev));
            if (Felhasznalonev == "Bela")
            {
                var f = new Felhasznalo() { FelhasznaloNev = "Bela", Jelszo = "1234", FelhasznaloId = 42, JogkorId = 1, Tetel = null };
                var j = new Jogkor()
                {
                    JogkorNev = "GOD",
                    JogkorId = 1,
                    Felhasznalo = new Felhasznalo[] { f }
                };
                f.Jogkor = j;
                return f;
            }
            else
                throw new AccessViolationException("Nincs ilyen felhasználó!");
        }

        Repository.FelhasznaloRepository f = new Repository.FelhasznaloRepository();


        public string ModifyFelhasznalo(int FelhasznaloID, string Felhasznalonev = null, string Jelszo = null, int? JogkorID = null)
        {

            try
            {
                f.Modify(1);
                return "SIEKR";

            }
            catch (Exception e)
            {

                return e.Message.ToString();
            }

        }
    


}

    public class JogkorLogic :ParentLogic, IJogkor
    {
        Repository.JogkorRepository j = new Repository.JogkorRepository();
        public IEnumerable<Jogkor> GetJogkor(int? JogkorId = null)
        {
            if (JogkorId == null)
            {
                return j.GetAll();
            }
            return new Jogkor[] { j.GetById(JogkorId.Value) };
        }
    }

    public class RaktarLogic :ParentLogic, IRaktar
    {
        Repository.RaktarRepository r = new Repository.RaktarRepository();
        public IEnumerable<Raktar> GetRaktar(int? RaktarID = null)
        {
            if (RaktarID == null)
            {
                return r.GetAll();
            }
            return new Raktar[] { r.GetById(RaktarID.Value) };
        }
    }

    public class SzallitLogic : ParentLogic, ISzallit
    {
        Repository.SzallitRepository sz = new Repository.SzallitRepository();
        public string AddSzallit(int SzallitasID, int TetelID, List<Data.Tetel> Tetel)
        {
            if (context.Szallitas.SingleOrDefault(x => x.SzallitasId == SzallitasID) == null)
            {
                throw new Exception("Nincs ilyen Szallitas");
            }
            if (context.Tetel.SingleOrDefault(x => x.TetelId == TetelID) == null)
            {
                throw new Exception("Nincs ilyen Tetel");
            }
            sz.Insert(new Szallit() { SzallitasId = SzallitasID, TetelId = TetelID });
            return "Sikerült a beszúrás";
        }

        public string DeleteSzallit(int SzallitID)
        {
            sz.Delete(SzallitID);
            return "Sikerült törölni";
        }

        public IEnumerable<Szallit> GetSzallit(int? SzallitID = null)
        {
            if (SzallitID == null)
            {
                return sz.GetAll();
            }
            return new Szallit[] { sz.GetById(SzallitID.Value) };

        }

        public string ModifySzallit(int SzallitID, int? SzallitasID = null, int? TetelID = null)
        {
            sz.Modify(SzallitID, SzallitasID, TetelID);
            return "Sikerüt a módosítás";
        }
    }

    public class SzallitasLogic : ParentLogic, ISzallitas
    {
        Repository.SzallitasRepository sz = new Repository.SzallitasRepository();
        public string AddSzallitas(DateTime Datum, int SzallitoID)
        {
            if (context.Szallitas.SingleOrDefault(x => x.SzallitoId == SzallitoID) == null)
            {
                throw new Exception("Nincs ilyen szállító");
            }
            sz.Insert(new Szallitas() { Datum = Datum, SzallitoId = SzallitoID });
            return "Sikeres beszrás";
        }

        public string DeleteSzallitas(int SzallitasID)
        {
            sz.Delete(SzallitasID);
            return "Sikeres törlés";
        }

        public IEnumerable<Szallitas> GetSzallitas(int? SzallitasID = null)
        {
            if (SzallitasID == null)
            {
                return sz.GetAll();
            }
            return new Szallitas[] { sz.GetById(SzallitasID.Value) };
        }

        public string ModifySzallitas(int SzallitasID, DateTime? Datum = null, int? SzallitoID = null)
        {
            sz.Modify(SzallitasID, Datum, SzallitoID);
            return "Módosítás kész";
        }
    }

    public class SzallitoLogic : ParentLogic, ISzallito
    {
        Repository.SzallitoRepository sz=new Repository.SzallitoRepository();
        public string AddSzallito(string SzallitoNev)
        {
            sz.Insert(new Szallito() { SzallitoNev = SzallitoNev });
            return "Beszúrás sikeres";
        }

        public string DeleteSzallito(int SzallitoID)
        {
            sz.Delete(SzallitoID);
            return "Törlés sikeres";
        }

        public IEnumerable<Szallito> GetSzallito(int? SzallitoID = null)
        {
            if (SzallitoID == null)
            {
                return sz.GetAll();
            }
            return new Szallito[] { sz.GetById(SzallitoID.Value) };
        }

        public string ModifySzallito(int SzallitoID, string SzallitoNev)
        {
            if (SzallitoNev == "")
            {
                throw new Exception("Nem jó szállító nevet adtál meg");
            }
            sz.Modify(SzallitoID, SzallitoNev);
            return "Módosítás sikeres";
        }
    }

    public class TartalmazLogic : ParentLogic, ITartalmaz
    {
        Repository.TartalmazRepository t = new Repository.TartalmazRepository();
        public string AddTartalmaz(int RaktarID, int Cikkszam, int Mennyiseg)
        {
            if (context.Raktar.SingleOrDefault(x => x.RaktarId == RaktarID) == null)
            {
                throw new Exception("Nincs ilyen Raktár");
            }
            if (context.Termek.SingleOrDefault(x => x.CikkSzam == Cikkszam) == null)
            {
                throw new Exception("Nincs ilyen termék");
            }
            if (Mennyiseg <= 0)
            {
                throw new Exception("Nem jó mennyiség (kisebb vagy egyenlő mint nulla)");
            }
            t.Insert(new Data.Tartalmaz() { RaktarId=RaktarID, CikkSzam=Cikkszam, Mennyiseg=Mennyiseg });
            return "Termék beszúrása sikerült";
        }

        public string DeleteTartalmaz(int TartalmazID)
        {
            t.Delete(TartalmazID);
            return "Törlés sikeres";
        }

        public IEnumerable<Data.Tartalmaz> GetTartalmaz(int? TartalmazID = null)
        {
            if (TartalmazID == null)
            {
                return t.GetAll();
            }
            return new Tartalmaz[] { t.GetById(TartalmazID.Value) };
        }

        public string ModifyTartalmaz(int TartalmazID, int? RaktarID = null, int? Cikkszam = null, int? Mennyiseg = null)
        {
            t.Modify(TartalmazID, RaktarID, Cikkszam, Mennyiseg);
            return "Módosítás sikeres";
        }
    }

    public class TermekLogic : ParentLogic, ITermek
    {
        Repository.Termek t = new Repository.Termek();
        public string AddTermek(int Cikkszam, int Vonalkod, string Termeknev, string TermekLeiras, int Ar)
        {
            if (context.Termek.SingleOrDefault(x => x.CikkSzam == Cikkszam) != null)
            {
                throw new Exception("Már létezik ilyen termék");
            }
            t.Insert(new Data.Termek { CikkSzam = Cikkszam, VonalKod = Vonalkod, TermekNev = Termeknev, TermekLeiras = TermekLeiras, Ar = Ar });
            return "Sikeres beszúrás";
        }

        public bool CikkszamEllenorzo(int Cikkszam)
        {
            if (context.Termek.SingleOrDefault(x => x.CikkSzam == Cikkszam) == null)
            {
                return false;
            }
            else return true;
        }

        public string DeleteTermek(int TermekID)
        {
            t.Delete(TermekID);
            return "Törölve";
        }

        public IEnumerable<Data.Termek> GetTermek(int? TermekID = null)
        {
            if (TermekID == null)
            {
               return t.GetAll();
            }
            return new Termek[] { t.GetById(TermekID.Value) };
        }

        public string ModifyTermek(int Cikkszam, int? Vonalkod = null, string Termeknev = null, string TermekLeiras = null, int? Ar = null)
        {
            //Mivel itt nincs idegen kulcs ezért csak az árat kell ellenőrizni, hogy ne legyen minusz
            if (Ar < 0)
            {
                throw new Exception("Nincs negatív ár!");
            }
            t.Modify(Cikkszam, Vonalkod, Termeknev, TermekLeiras, Ar);
            return "Sikeres módosítás";

        }
    }

    public class TetelLogic : ParentLogic, ITetel
    {
        Repository.TetelRepository t = new Repository.TetelRepository();

        public string AddTetel(int Cikkszam, int FelhasznaloID, int RaktarID, int Mennyiseg)
        {
            if (context.Felhasznalo.SingleOrDefault(x => x.FelhasznaloId == FelhasznaloID) == null)
            {
                throw new Exception("Nincs ilyen felhasználó");
            }
            t.Insert(new Data.Tetel() { CikkSzam = Cikkszam, FelhasznaloId = FelhasznaloID, RaktarId = RaktarID, Mennyiseg = Mennyiseg });
                return "sikreült";
        }

        public string DeleteTetel(int TetelID)
        {
            t.Delete(TetelID);
            return "Törölve";
        }

        public IEnumerable<Data.Tetel> GetTetel(int? TetelID = null)
        {
            if (TetelID == null)
            {
                return t.GetAll();
            }
            return new Tetel[] { t.GetById(TetelID.Value) };
        }

        public string ModifyTetel(int TetelID, int? Cikkszam = null, int? FelhasznaloID = null, int? RaktarID = null, int? Mennyiseg = null)
        {
            if (context.Tetel.SingleOrDefault(x => x.TetelId == TetelID) == null)
            {
                throw new Exception("Nincs ilyen Tétel");
            }
            if (context.Termek.SingleOrDefault(x => x.CikkSzam == Cikkszam) == null)
            {
                throw new Exception("Nincs ilyen Cikkszám");
            }
            if (context.Felhasznalo.SingleOrDefault(x => x.FelhasznaloId == FelhasznaloID) == null)
            {
                throw new Exception("Nincs ilyen felhasználó");
            }
            if (context.Raktar.SingleOrDefault(x => x.RaktarId == RaktarID) == null)
            {
                throw new Exception("Nincs ilyen raktár");
            }
            t.Modify(TetelID, Cikkszam, FelhasznaloID, RaktarID, Mennyiseg);
            return "Tétel módosítva";
        }

  
    }

}
*/
