﻿// <copyright file="BeszallitoHozzaadasModositas.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BL;
    using Data;
    using L1dl_WPF.ViewModel;

    /// <summary>
    /// Interaction logic for BeszallitoHozzaadasModositas.xaml
    /// </summary>
    public partial class BeszallitoHozzaadasModositas : Window
    {
        private IBeszallito logic = new BeszallitoLogic();
        private BeszallitoHozzaadasModositasVM vm;
        private List<Data.Termek> list = new List<Data.Termek>();
        private bool isnew;

        private HomeVM homeVM = new HomeVM();

        public BeszallitoHozzaadasModositas(BeszallitoHozzaadasModositasVM viewmodel, bool isnew = true)
        {
            this.InitializeComponent();
            this.DataContext = viewmodel;
            this.vm = viewmodel;
            this.isnew = isnew;
            if (this.lb_termekek.Items.Count == 0)
            {
                this.btn_termekBeszallitoListaElemTorol.IsEnabled = true;
            }
            else
            {
                this.btn_termekBeszallitoListaElemTorol.IsEnabled = false;
            }
        }

        /// <summary>
        /// Teszt gomb
        /// </summary>
        public Button TesztButton
        {
            get { return this.btn_termekBeszallitoListaElemTorol; }
        }

        /// <summary>
        /// Beszállító Listabeszúráért felelős klikk metódus
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        public void btn_termekBeszallitoListabaszur_Click(object sender, RoutedEventArgs e)
        {
            if (!this.vm.BeszallitoTermekeiList.Items.Contains(this.cb_beszallitoTermeke.SelectedItem))
            {
                this.vm.BeszallitoTermekeiList.Items.Add(this.cb_beszallitoTermeke.SelectedItem);
                this.btn_termekBeszallitoListaElemTorol.IsEnabled = true;
            }
        }

        /// <summary>
        /// Beszallito módosítása klikk metódus
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private async void btn_beszallitoHozzaadModosit_Click(object sender, RoutedEventArgs e)
        {
            if (this.isnew)
            {
                List<int> ser = new List<int>();
                foreach (var item in this.lb_termekek.Items)
                {
                    ser.Add(((dynamic)item).Cikkszám);
                }

                var termekek = await new TermekLogic().GetTermek();
                foreach (var item in ser)
                {
                    this.list.Add(termekek.SingleOrDefault(q => q.CikkSzam == item));
                }

                await this.logic.AddBeszallito(this.tb_beszallitoNeve.Text, this.list);
                var x = this.logic.GetBeszallito();

                this.homeVM = new HomeVM();
                await this.homeVM.Listazas();
                Window home = new View.Home();

                this.Close();
                home.ShowDialog();
            }
            else
            {
                List<int> ser = new List<int>();
                foreach (var item in this.lb_termekek.Items)
                {
                    ser.Add(((dynamic)item).Cikkszám);
                }

                var termekek = await new TermekLogic().GetTermek();
                foreach (var item in ser)
                {
                    this.list.Add(termekek.SingleOrDefault(q => q.CikkSzam == item));
                }

                await this.logic.ModifyBeszallito(this.vm?.List?.ElementAt(0)?.BeszállítóID, this.tb_beszallitoNeve?.Text, this.list);

                this.homeVM = new HomeVM();
                Window home = new View.Home();

                this.Close();
                home.ShowDialog();
            }
        }

        /// <summary>
        /// Termék Beszállító listából való törlése
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void btn_termekBeszallitoListaElemTorol_Click(object sender, RoutedEventArgs e)
        {
            if (this.lb_termekek.SelectedItem != null)
            {
                this.vm.BeszallitoTermekeiList.Items.Remove(this.lb_termekek.SelectedItem);
                if (this.vm.BeszallitoTermekeiList.Items.Count == 0)
                {
                    this.btn_termekBeszallitoListaElemTorol.IsEnabled = false;
                }
            }
        }

        /// <summary>
        /// Ablak bezárása
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void Window_Closed(object sender, EventArgs e)
        {
            Window home = new View.Home();
            home.ShowDialog();
        }

        /// <summary>
        /// Kattintásra cancle
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            Home h = new Home();
            this.Close();
            h.ShowDialog();
        }
    }
}
