﻿// <copyright file="Home.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF.View
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Runtime.InteropServices;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Interop;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BL;
    using L1dl_WPF.ViewModel;

    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : Window
    {
        private HomeVM homeVM = new HomeVM();
        private TermekHozzaadasModositasVM thmVM;
        private RendelesVM rendelesVM;
        private BeszallitoHozzaadasModositasVM beszallitoVM;
        private BL.ITermek logic = new TermekLogic();
        private BL.IBeszallito beszallitoLogic = new BeszallitoLogic();
        private BL.IRendeles szallitasLogic = new RendelesLogic();
        private IFelhasznalo flogic = new FelhasznaloLogic();
        private Visibility hidden = Visibility.Hidden;
        private Visibility visible = Visibility.Visible;
        private List<dynamic> torlendoList = new List<dynamic>();
        private string eventRoute;
        private double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
        private double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;

        /// <summary>
        /// Initializes a new instance of the <see cref="Home"/> class.
        /// Home konstruktora
        /// </summary>
        /// <param name="f">felhasználó</param>
        public Home(Data.Felhasznalo f)
        {
            this.InitializeComponent();
            HomeVM.Aktualis = f;
            this.ti_raktarKezeles_GotFocus(null, null);
            this.WindowStyle = WindowStyle.None;
            this.Left = (this.screenWidth / 2) - (this.Width / 2);
            this.Top = (this.screenHeight / 2) - (this.Height / 2);
            this.tc_tabok.SelectedIndex = HomeVM.Aktualis.UtolsoAblak;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Home"/> class.
        /// Paraméter nélküli Home konstruktor létrehozása
        /// </summary>
        public Home()
        {
            this.InitializeComponent();
            this.ti_raktarKezeles_GotFocus(null, null);
            this.WindowStyle = WindowStyle.None;
            this.Left = (this.screenWidth / 2) - (this.Width / 2);
            this.Top = (this.screenHeight / 2) - (this.Height / 2);
            this.tc_tabok.SelectedIndex = HomeVM.Aktualis.UtolsoAblak;
        }

        /// <summary>
        /// Gomb elrejtő metódus
        /// </summary>
        public void HideBtns()
        {
            this.btn_uj.Visibility = this.hidden;
            this.btn_modosit.Visibility = this.hidden;
            this.btn_torol.Visibility = this.hidden;
        }

        /// <summary>
        /// Gomb megmutató metódus
        /// </summary>
        public void ShowBtns()
        {
            this.btn_uj.Visibility = this.visible;
            this.btn_modosit.Visibility = this.visible;
            this.btn_torol.Visibility = this.visible;
        }

        private void btn_uj_Click(object sender, RoutedEventArgs e)
        {
            HomeVM.Aktualis.UtolsoAblak = this.tc_tabok.SelectedIndex;
            switch (this.tc_tabok.SelectedIndex)
            {
                case 0:
                    this.eventRoute = "uj";
                    this.thmVM = new TermekHozzaadasModositasVM(this.homeVM.TermekListForBinding, this.eventRoute);
                    TermekHozzaadasModositas termekHozzaadModositWindow = new TermekHozzaadasModositas(this.thmVM);
                    this.Visibility = Visibility.Collapsed;
                    termekHozzaadModositWindow.ShowDialog();
                    this.Visibility = Visibility.Visible;
                    break;
                case 1:
                    this.eventRoute = "uj";
                    this.rendelesVM = new RendelesVM(this.homeVM.RendelesList_forBinding, this.eventRoute, -1, HomeVM.Aktualis, this.homeVM);
                    Rendeles rendelesWindow = new Rendeles(this.rendelesVM);
                    this.Close();
                    rendelesWindow.ShowDialog();
                    break;
                case 2:
                    this.eventRoute = "uj";
                    this.beszallitoVM = new BeszallitoHozzaadasModositasVM(this.homeVM.BeszallitoListForBinding, this.eventRoute);
                    BeszallitoHozzaadasModositas beszallitoWindow = new BeszallitoHozzaadasModositas(this.beszallitoVM);
                    this.Close();
                    beszallitoWindow.ShowDialog();
                    break;
                default:
                    break;
            }
        }

        private void btn_modosit_Click(object sender, RoutedEventArgs e)
        {
            HomeVM.Aktualis.UtolsoAblak = this.tc_tabok.SelectedIndex;
            switch (this.tc_tabok.SelectedIndex)
            {
                case 0:
                    if (this.dg_raktar.SelectedItem == null)
                    {
                        MessageBox.Show("Nincs kiválasztva módosítandó elem!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    this.eventRoute = "modosit";
                    this.thmVM = new TermekHozzaadasModositasVM(this.homeVM.TermekListForBinding, this.eventRoute);
                    TermekHozzaadasModositas termekHozzaadModositWindow = new TermekHozzaadasModositas(this.thmVM);
                    this.Close();
                    termekHozzaadModositWindow.ShowDialog();
                    break;
                case 1:
                    this.eventRoute = "modosit";
                    if (this.dg_rendelesek.SelectedItem == null)
                    {
                        MessageBox.Show("Nincs kiválasztva elem!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    this.rendelesVM = new RendelesVM((dynamic)this.homeVM.RendelesList_forBinding, this.eventRoute, ((dynamic)this.homeVM.RendelesList_forBinding.ElementAt(0)).RendelésID, HomeVM.Aktualis, this.homeVM);
                    Rendeles rendelesWindow = new Rendeles(this.rendelesVM);
                    this.Close();
                    rendelesWindow.ShowDialog();
                    break;
                case 2:
                    if (this.dg_beszallitok.SelectedItem == null)
                    {
                        MessageBox.Show("Nincs kiválasztva módosítandó elem!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    this.eventRoute = "modosit";
                    this.beszallitoVM = new BeszallitoHozzaadasModositasVM(this.homeVM.BeszallitoListForBinding, this.eventRoute);
                    BeszallitoHozzaadasModositas beszallitoWindow = new BeszallitoHozzaadasModositas(this.beszallitoVM, false);
                    this.Close();
                    beszallitoWindow.ShowDialog();
                    break;
                default:
                    break;
            }
        }

        private void ti_raktarKezeles_GotFocus(object sender, RoutedEventArgs e)
        {
            this.btn_uj.IsEnabled = true;
            this.btn_torol.IsEnabled = true;
            this.btn_modosit.IsEnabled = true;
            this.ShowBtns();
            this.btn_uj.Content = "Termék felvétele";
            this.btn_modosit.Content = "Termék módosítása";
            this.btn_torol.Content = "Termék törlése";

            if (HomeVM.Aktualis.JogkorId == 1)
            {
                this.btn_uj.Visibility = Visibility.Collapsed;
                this.btn_modosit.Visibility = Visibility.Collapsed;
                this.btn_torol.Visibility = Visibility.Collapsed;
            }
        }

        private void ti_szallitasKezeles_GotFocus(object sender, RoutedEventArgs e)
        {
            this.btn_uj.IsEnabled = true;
            this.btn_torol.IsEnabled = true;
            this.btn_modosit.IsEnabled = true;
            this.ShowBtns();
            this.btn_uj.Content = "Rendelés felvétele";
            this.btn_modosit.Content = "Rendelés részletei";
            this.btn_torol.Content = "Rendelés törlése";
            if (HomeVM.Aktualis.JogkorId == 1)
            {
                this.btn_uj.Visibility = Visibility.Collapsed;
                this.btn_modosit.Visibility = Visibility.Collapsed;
                this.btn_torol.Visibility = Visibility.Collapsed;
            }

            foreach (var item in this.dg_rendelesek.Columns)
            {
                if (item.Header.ToString() == "OlyanMintCsakNem")
                {
                    item.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void ti_beszallitoKezeles_GotFocus(object sender, RoutedEventArgs e)
        {
            this.btn_uj.IsEnabled = true;
            this.btn_torol.IsEnabled = true;
            this.btn_modosit.IsEnabled = true;
            this.ShowBtns();

            if (HomeVM.Aktualis.JogkorId < 3)
            {
               this.btn_uj.Visibility = Visibility.Collapsed;
               this.btn_modosit.Visibility = Visibility.Collapsed;
               this.btn_torol.Visibility = Visibility.Collapsed;
            }

            foreach (var item in this.dg_beszallitok.Columns)
            {
                if (item.Header.ToString() == "Termékei")
                {
                    item.Visibility = Visibility.Collapsed;
                }
            }

            this.btn_uj.Content = "Beszállító felvétele";
            this.btn_modosit.Content = "Beszállító módosítása";
            this.btn_torol.Content = "Beszállító törlése";
        }

        private void ti_userKezeles_GotFocus(object sender, RoutedEventArgs e)
        {
            this.btn_uj.IsEnabled = false;
            this.btn_torol.IsEnabled = false;
            this.btn_modosit.IsEnabled = false;
            this.HideBtns();
            this.btn_uj.Content = string.Empty;
            this.btn_modosit.Content = string.Empty;
            this.btn_torol.Content = string.Empty;
        }

        private async void btn_torol_Click(object sender, RoutedEventArgs e)
        {
            if (this.tc_tabok.SelectedIndex == 0 && this.dg_raktar.SelectedItem == null)
            {
                MessageBox.Show("Nincs kiválasztva törlendő elem!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (this.tc_tabok.SelectedIndex == 1 && this.dg_rendelesek.SelectedItem == null)
            {
                MessageBox.Show("Nincs kiválasztva törlendő elem!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (this.tc_tabok.SelectedIndex == 2 && this.dg_beszallitok.SelectedItem == null)
            {
                MessageBox.Show("Nincs kiválasztva törlendő elem!", "ERROR", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            var result = MessageBox.Show("Biztosan törli?", "Elem törlése", MessageBoxButton.YesNo);
            if (result == MessageBoxResult.Yes && this.tc_tabok.SelectedIndex == 0)
            {
                this.torlendoList.Clear();
                this.torlendoList.Add(this.dg_raktar.SelectedItem);
                await this.logic.DeleteTermek((dynamic)this.torlendoList.ElementAt(0).Cikkszám);
                this.homeVM.Termekek.Remove((dynamic)this.torlendoList.ElementAt(0));
                var x = await this.logic.GetTermek();
                this.dg_raktar.ItemsSource = null;
                this.dg_raktar.ItemsSource = this.homeVM.Termekek;
            }

            if (result == MessageBoxResult.Yes && this.tc_tabok.SelectedIndex == 1)
            {
                this.torlendoList.Clear();
                this.torlendoList.Add(this.dg_rendelesek.SelectedItem);
                var d = (dynamic)this.torlendoList.ElementAt(0).RendelésID;
                await this.szallitasLogic.DeleteRendeles((dynamic)this.torlendoList.ElementAt(0).RendelésID);
                this.homeVM.Rendelesek.Remove((dynamic)this.torlendoList.ElementAt(0));
                var r = await this.szallitasLogic.GetRendeles();
                List<dynamic> asdsa = new List<dynamic>();
                foreach (var item in this.homeVM.Rendelesek)
                {
                    asdsa.Add(item);
                }

                this.homeVM.Rendelesek = asdsa;
                this.dg_rendelesek.ItemsSource = null;
                this.dg_rendelesek.ItemsSource = this.homeVM.Rendelesek;
                foreach (var item in this.dg_rendelesek.Columns)
                {
                    if (item.Header.ToString() == "OlyanMintCsakNem")
                    {
                        item.Visibility = Visibility.Collapsed;
                    }
                }
            }

            if (result == MessageBoxResult.Yes && this.tc_tabok.SelectedIndex == 2)
            {
                this.torlendoList.Clear();
                this.torlendoList.Add(this.dg_beszallitok.SelectedItem);
                await this.beszallitoLogic.DeleteBeszallito((dynamic)this.torlendoList.ElementAt(0).BeszállítóID);
                this.homeVM.Beszallitok.Remove((dynamic)this.torlendoList.ElementAt(0));
                this.dg_beszallitok.ItemsSource = null;
                this.dg_beszallitok.ItemsSource = this.homeVM.Beszallitok;
                this.ti_beszallitoKezeles_Loaded(null, null);
            }
        }

        private void dg_raktar_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.homeVM.Dg = this.dg_raktar;
            this.homeVM.SelectedDatagridItem = this.homeVM.Dg.SelectedItem;
            if (this.homeVM.TermekList_forBinding != null)
            {
                this.homeVM.TermekList_forBinding.Clear();
            }

            this.homeVM.TermekList_forBinding.Add(this.homeVM.SelectedDatagridItem);
        }

        private void dg_beszallitok_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.homeVM.Dg = this.dg_beszallitok;
            this.homeVM.SelectedBeszallitoDatagridItem = this.homeVM.Dg.SelectedItem;
            if (this.homeVM.BeszallitoList_forBinding != null)
            {
                this.homeVM.BeszallitoList_forBinding.Clear();
            }

            this.homeVM.BeszallitoList_forBinding.Add(this.homeVM.SelectedBeszallitoDatagridItem);
        }

        private void dg_rendelesek_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.homeVM.Dg = this.dg_rendelesek;
            this.homeVM.SelectedRendelesDatagridItem = this.homeVM.Dg.SelectedItem;
            if (this.homeVM.RendelesList_forBinding != null)
            {
                this.homeVM.RendelesList_forBinding.Clear();
            }

            this.homeVM.RendelesList_forBinding.Add(this.homeVM.SelectedRendelesDatagridItem);
        }

        private async void btn_pwChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                await this.flogic.ChangePassword(HomeVM.Aktualis.FelhasznaloId, this.tb_dolgozoPW_old.Password, this.tb_dolgozoPW.Password);

                MessageBox.Show("Sikeres jelszómódosítás!");
                this.tb_dolgozoPW.Password = string.Empty;
                this.tb_dolgozoPW_confirm.Password = string.Empty;
                this.tb_dolgozoPW_old.Password = string.Empty;
                this.tc_tabok.SelectedIndex = 0;
            }
            catch (Exception)
            {
                MessageBox.Show("Érvénytelen régi jelszó!", " ", MessageBoxButton.OK, MessageBoxImage.Error);
                this.tb_dolgozoPW_old.Password = string.Empty;
            }
        }

        private async void btn_exit_Click(object sender, RoutedEventArgs e)
        {
            IFelhasznalo f = new FelhasznaloLogic();
            await f.ModifyFelhasznalo(HomeVM.Aktualis.FelhasznaloId, utolsoAblak: this.tc_tabok.SelectedIndex);
            Environment.Exit(0);
        }

        private void tb_dolgozoPW_confirm_LostFocus(object sender, RoutedEventArgs e)
        {
            if (this.tb_dolgozoPW.Password != this.tb_dolgozoPW_confirm.Password)
            {
                this.warning.Visibility = Visibility.Visible;
                this.btn_pwChange.IsEnabled = false;
                this.warning.Content = "Nem egyezik meg a két jelszó!";
                this.warning.Foreground = Brushes.Red;
                this.tb_dolgozoPW_confirm.BorderBrush = Brushes.Red;
                this.tb_dolgozoPW_confirm.BorderThickness = new Thickness(2);
            }
            else
            {
                this.btn_pwChange.IsEnabled = true;
                this.tb_dolgozoPW_confirm.BorderThickness = new Thickness(0);
                this.warning.Visibility = Visibility.Collapsed;
            }
        }

        private void ti_beszallitoKezeles_Loaded(object sender, RoutedEventArgs e)
        {
            foreach (var item in this.dg_beszallitok.Columns)
            {
                if (item.Header.ToString() == "Termékei")
                {
                    item.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void ti_beszallitoKezeles_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            this.ti_beszallitoKezeles_Loaded(null, null);
        }

        private void dg_beszallitok_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.ti_beszallitoKezeles_Loaded(null, null);
        }
    }
}
