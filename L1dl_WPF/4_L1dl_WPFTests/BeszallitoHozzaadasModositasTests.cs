﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using L1dl_WPF.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace L1dl_WPF.View.Tests
{
    [TestClass()]
    public class BeszallitoHozzaadasModositasTests
    {
        [TestMethod()]
        public void btn_termekBeszallitoListabaszur_ClickTest()
        {
            BeszallitoHozzaadasModositas window = new BeszallitoHozzaadasModositas(new ViewModel.BeszallitoHozzaadasModositasVM(null,"uj"));

            var old = window.TesztButton.IsEnabled;
            window.btn_termekBeszallitoListabaszur_Click(null, null);
            var newasd = window.TesztButton.IsEnabled;
            Assert.AreEqual(old, newasd);
        }
    }
}