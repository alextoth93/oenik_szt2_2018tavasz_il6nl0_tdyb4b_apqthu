﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using L1dl_WPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1dl_WPF.ViewModel.Tests
{
    [TestClass()]
    public class BeszallitoHozzaadasModositasVMTests
    {
        [TestMethod()]
        public void BeszallitoHozzaadasModositasVMTest()
        {
            List<dynamic> list = new List<dynamic>();
            /*
            bool log = false;
            try
            {
                BeszallitoHozzaadasModositasVM vm = new BeszallitoHozzaadasModositasVM(list, "uj");
            }
            catch (Exception)
            {
                log = true;
                
            }
            Assert.IsTrue(log);
            */
            BeszallitoHozzaadasModositasVM vm = new BeszallitoHozzaadasModositasVM(list, "uj");
            Assert.IsNotNull(vm.TermekLista);
            Assert.AreNotEqual(0, vm.TermekLista.Count());
        }
    }
}