﻿// <copyright file="FelhasznaloLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Security.Cryptography;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Repository;

    /// <summary>
    /// Felhasználó logicját leíró osztály
    /// </summary>
    public class FelhasznaloLogic : ParentLogic, IFelhasznalo
    {
        private IFelhasznaloRepo repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="FelhasznaloLogic"/> class.
        /// Paraméter nélküli konstruktora
        /// </summary>
        public FelhasznaloLogic()
        {
            this.repo = new FelhasznaloRepo();
        }

        /// <summary>
        /// Jelszó változtatása
        /// </summary>
        /// <param name="felhasznaloID">id</param>
        /// <param name="oldJelszo">régi</param>
        /// <param name="newJelszo">új</param>
        /// <returns>válasz</returns>
        public async Task<string> ChangePassword(int felhasznaloID, string oldJelszo, string newJelszo)
        {
            var user = await this.repo.GetById(felhasznaloID);
            if (user == null)
            {
                throw new ArgumentException("Ismeretlen hiba!");
            }

            oldJelszo = this.GenerateHash(oldJelszo);
            newJelszo = this.GenerateHash(newJelszo);
            if (user.Jelszo != oldJelszo)
            {
                throw new ArgumentException("Érvénytelen régi jelszó!");
            }

            await this.repo.ModifyFelhasznalo(user.FelhasznaloId, jelszo: newJelszo);
            return "Sikeres jelszómódosítás!";
        }

        /// <summary>
        /// Get felhasználó
        /// </summary>
        /// <param name="felhasznaloID">egy id(intet) várunk</param>
        /// <returns>visszakapjuk a felhasználót</returns>
        public async Task<Felhasznalo> GetFelhasznalo(int felhasznaloID)
        {
            return await Task.Factory.StartNew(() =>
            {
                var user = this.repo.GetById(felhasznaloID)?.Result;
                if (user == null || user.ToroltE == "Y")
                {
                    throw new AccessViolationException("Érvénytelen belépés!");
                }
                return user;
            });
            }

        /// <summary>
        /// Login metódusa
        /// </summary>
        /// <param name="felhasznalonev">felhasználó</param>
        /// <param name="jelszo">jelszó</param>
        /// <returns>felhasználót visszakpjuk</returns>
        public async Task<Felhasznalo> Login(string felhasznalonev, string jelszo)
        {
            jelszo = this.GenerateHash(jelszo);
            var user = await this.repo.GetByNevAndJelszo(felhasznalonev, jelszo);
            if (user == null || user.ToroltE == "Y")
            {
                throw new AccessViolationException("Érvénytelen adatok!");
            }

            await this.repo.ModifyFelhasznalo(user.FelhasznaloId, utolsoBelepes: DateTime.Now);
            return user;
        }

        /// <summary>
        /// Felhasználó módosítása
        /// </summary>
        /// <param name="felhasznaloID">egy intet várunk id-nak</param>
        /// <param name="felhasznalonev">felhasználó neve</param>
        /// <param name="jogkorID">jogköre</param>
        /// <param name="utolsoAblak">utolsó ablak</param>
        /// <returns>válasz</returns>
        public async Task<string> ModifyFelhasznalo(int felhasznaloID, string felhasznalonev = null, int? jogkorID = null, int? utolsoAblak = null)
        {
            if (jogkorID != null)
            {
                if (new JogkorRepo().GetById(jogkorID.Value) == null)
                {
                    throw new ArgumentException("Érvénytelen jogkörID!");
                }
            }

            try
            {
                await this.repo.ModifyFelhasznalo(felhasznaloID, nev: felhasznalonev, jogkorID: jogkorID, utolsoAblak: utolsoAblak);
                return "Sikeres módosítás";
            }
            catch (Exception)
            {
                return "Ismeretlen hiba történt!";
            }
        }

        /// <summary>
        /// Hash generálása
        /// </summary>
        /// <param name="s">egy paraméter amit átalakítunk</param>
        /// <returns>egy string</returns>
        public string GenerateHash(string s)
        {
            // return s.GetHashCode().ToString();
            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(s);
            byte[] hash = sha256.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        /// <summary>
        /// Megkapuk a stringet a hashből
        /// </summary>
        /// <param name="hash">hash</param>
        /// <returns>vissza kapunk egy hashelt stringet</returns>
        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }

            return result.ToString();
        }
    }
}
