﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using L1dl_WPF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1dl_WPF.Tests
{
    [TestClass()]
    public class MainWindowTests
    {
        [TestMethod()]
        public void btn_login_ClickTest_wrong_user()
        {
            MainWindow w = new MainWindow();
            w.Name = "ROSSZ USER";
            w.Passw = "1234";
            Task t = new Task(()=>w.btn_login_Click(null, null));
            t.RunSynchronously();
            Assert.IsNull(w.F);
        }
    }
}