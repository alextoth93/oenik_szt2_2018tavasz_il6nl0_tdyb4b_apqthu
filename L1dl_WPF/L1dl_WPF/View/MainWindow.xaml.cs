﻿// <copyright file="MainWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using L1dl_WPF.View;
    using L1dl_WPF.ViewModel;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Data.Felhasznalo F;
        private ViewModel.MainWindowVM vM;
        private Window homeWindow;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// paraméter nélküli kontruktor
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.vM = new ViewModel.MainWindowVM();
        }

        /// <summary>
        /// Név
        /// </summary>
        public new string Name
        {
            get { return this.tb_dolgozoID.Text; } set { this.tb_dolgozoID.Text = value; }
        }

        /// <summary>
        /// Jelszó
        /// </summary>
        public string Passw
        {
            get { return this.tb_dolgozoPW.Password; } set { this.tb_dolgozoPW.Password = value; }
        }

        /// <summary>
        /// Belépés klikkelése
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        public async void btn_login_Click(object sender, EventArgs e)
        {
            try
            {
                this.F = await this.vM.Login(this.tb_dolgozoID.Text, this.tb_dolgozoPW.Password);
            }
            catch (AccessViolationException)
            {
                MessageBox.Show("Érvénytelen felhasználónév/jelszó!", string.Empty, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (this.F == null)
            {
                MessageBox.Show("Ismeretlen hiba!");
                this.Close();
            }

            this.homeWindow = new View.Home(this.F);
            Application.Current.MainWindow.Close();
            this.homeWindow.ShowDialog();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.btn_login.Focus();
                this.btn_login_Click(sender, null);
            }
        }
    }
}
