﻿// <copyright file="IFelhasznaloRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IFelhasznaloRepo : IGenericRepository<Data.Felhasznalo>
    {
        Task<Data.Felhasznalo> GetByNevAndJelszo(string nev, string passw);

        Task ModifyFelhasznalo(int felhasznaloID, string nev = null, string jelszo = null, int? jogkorID = null, DateTime? utolsoBelepes = null, int? utolsoAblak = null);
    }
}
