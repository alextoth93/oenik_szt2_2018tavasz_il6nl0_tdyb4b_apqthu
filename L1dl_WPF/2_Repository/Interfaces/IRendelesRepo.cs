﻿// <copyright file="IRendelesRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IRendelesRepo : IGenericRepository<Data.Rendeles>
    {
        Task ModifyRendeles(int rendelesId, int? beszallitoId = null, DateTime? datum = null, int? felelosId = null);
    }
}
