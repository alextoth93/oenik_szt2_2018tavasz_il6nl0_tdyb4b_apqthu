var searchData=
[
  ['ibeszallito',['IBeszallito',['../interface_b_l_1_1_i_beszallito.html',1,'BL']]],
  ['ibeszallitorepo',['IBeszallitoRepo',['../interface_repository_1_1_i_beszallito_repo.html',1,'Repository']]],
  ['ibeszallitotermekconnectorrepo',['IBeszallitoTermekConnectorRepo',['../interface_repository_1_1_i_beszallito_termek_connector_repo.html',1,'Repository']]],
  ['ifelhasznalo',['IFelhasznalo',['../interface_b_l_1_1_i_felhasznalo.html',1,'BL']]],
  ['ifelhasznalorepo',['IFelhasznaloRepo',['../interface_repository_1_1_i_felhasznalo_repo.html',1,'Repository']]],
  ['igenericrepository',['IGenericRepository',['../interface_repository_1_1_i_generic_repository.html',1,'Repository']]],
  ['igenericrepository_3c_20data_2ebeszallito_20_3e',['IGenericRepository&lt; Data.Beszallito &gt;',['../interface_repository_1_1_i_generic_repository.html',1,'Repository']]],
  ['igenericrepository_3c_20data_2ebeszallitotermekconnector_20_3e',['IGenericRepository&lt; Data.BeszallitoTermekConnector &gt;',['../interface_repository_1_1_i_generic_repository.html',1,'Repository']]],
  ['igenericrepository_3c_20data_2efelhasznalo_20_3e',['IGenericRepository&lt; Data.Felhasznalo &gt;',['../interface_repository_1_1_i_generic_repository.html',1,'Repository']]],
  ['igenericrepository_3c_20data_2ejogkor_20_3e',['IGenericRepository&lt; Data.Jogkor &gt;',['../interface_repository_1_1_i_generic_repository.html',1,'Repository']]],
  ['igenericrepository_3c_20data_2erendeles_20_3e',['IGenericRepository&lt; Data.Rendeles &gt;',['../interface_repository_1_1_i_generic_repository.html',1,'Repository']]],
  ['igenericrepository_3c_20data_2etermek_20_3e',['IGenericRepository&lt; Data.Termek &gt;',['../interface_repository_1_1_i_generic_repository.html',1,'Repository']]],
  ['igenericrepository_3c_20data_2etetel_20_3e',['IGenericRepository&lt; Data.Tetel &gt;',['../interface_repository_1_1_i_generic_repository.html',1,'Repository']]],
  ['ijogkor',['IJogkor',['../interface_b_l_1_1_i_jogkor.html',1,'BL']]],
  ['ijogkorrepo',['IJogkorRepo',['../interface_repository_1_1_i_jogkor_repo.html',1,'Repository']]],
  ['irendeles',['IRendeles',['../interface_b_l_1_1_i_rendeles.html',1,'BL']]],
  ['irendelesrepo',['IRendelesRepo',['../interface_repository_1_1_i_rendeles_repo.html',1,'Repository']]],
  ['itermek',['ITermek',['../interface_b_l_1_1_i_termek.html',1,'BL']]],
  ['itermekrepo',['ITermekRepo',['../interface_repository_1_1_i_termek_repo.html',1,'Repository']]],
  ['itetel',['ITetel',['../interface_b_l_1_1_i_tetel.html',1,'BL']]],
  ['itetelrepo',['ITetelRepo',['../interface_repository_1_1_i_tetel_repo.html',1,'Repository']]]
];
