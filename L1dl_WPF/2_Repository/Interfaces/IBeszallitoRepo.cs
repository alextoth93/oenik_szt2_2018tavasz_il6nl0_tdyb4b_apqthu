﻿// <copyright file="IBeszallitoRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IBeszallitoRepo : IGenericRepository<Data.Beszallito>
    {
         Task ModifyBeszallito(int beszallitoId, string nev = null, string isDeleted = null);
    }
}
