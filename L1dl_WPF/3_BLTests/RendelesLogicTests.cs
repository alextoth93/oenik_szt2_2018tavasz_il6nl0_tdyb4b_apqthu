﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;

namespace BL.Tests
{
    [TestClass()]
    public class RendelesLogicTests
    {

        [TestMethod()]
        public void AddRendelesTest()
        {
            RendelesLogic logic = new RendelesLogic();
            List<Tetel> list = new List<Tetel>()
            {
                new Tetel(){Cikkszam=10000, Mennyiseg=10},
                new Tetel(){Cikkszam=10001, Mennyiseg=10}
            };
            var r = logic.AddRendeles(1, DateTime.Now, 1, list).Result;
            Assert.AreEqual(r, "Sikeresen hozzáadva");
            var d = logic.GetRendeles().Result;
        }



        [TestMethod()]
        public void GetRendelesTest()
        {
            RendelesLogic logic = new RendelesLogic();
            var r = logic.GetRendeles().Result;
            Assert.IsNotNull(r);
        }

        [TestMethod()]
        public void GetRendelesTest_single()
        {
            RendelesLogic logic = new RendelesLogic();
            var r = logic.GetRendeles(1).Result;
            Assert.IsNotNull(r);
            Assert.AreEqual(r.Count, 1);
        }

        [TestMethod()]
        public void ModifyRendelesTest()
        {
            RendelesLogic logic = new RendelesLogic();
            var r = logic.ModifyRendeles(1, datum: DateTime.UtcNow, felelosSzemely: 1).Result;
            Assert.AreEqual(r, "Sikeresen módosítva");
            var d = logic.GetRendeles().Result;
        }
    }
}