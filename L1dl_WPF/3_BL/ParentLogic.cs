﻿// <copyright file="ParentLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Szülő logica
    /// </summary>
    public class ParentLogic
    {
#pragma warning disable SA1401 // Fields must be private
        protected Data.Model context;
#pragma warning restore SA1401 // Fields must be private

        /// <summary>
        /// Initializes a new instance of the <see cref="ParentLogic"/> class.
        /// paraméter nélküli konstruktura
        /// </summary>
        public ParentLogic()
        {
            this.context = new Data.Model();
        }
    }
}
