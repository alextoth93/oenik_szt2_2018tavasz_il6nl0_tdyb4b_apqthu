﻿// <copyright file="RendelesRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

   public class RendelesRepo : GenericRepository<Rendeles>, IRendelesRepo
    {
        public async override Task<Rendeles> GetById(int id)
        {
            return await Task.Factory.StartNew(() => { return (from q in this.Context.Rendeles where q.RendelesId == id select q).SingleOrDefault(); });
        }

        public async Task ModifyRendeles(int rendelesId, int? beszallitoId = null, DateTime? datum = null, int? felelosId = null)
        {
            var old = await this.GetById(rendelesId);

            // var d = this.Context.Rendeles.ToList();
            if (old == null)
            {
                throw new ArgumentException("Nincs ilyen rendelés");
            }

            if (beszallitoId != null)
            {
                old.BeszallitoId = beszallitoId.Value;
            }

            if (datum != null)
            {
                old.Datum = datum.Value;
            }

            if (felelosId != null)
            {
                old.FelelosSzemelyId = felelosId.Value;
            }

            await this.Context.SaveChangesAsync();
        }
    }
}
