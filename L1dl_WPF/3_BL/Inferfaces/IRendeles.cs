﻿// <copyright file="IRendeles.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Rendelésekért felelős interface
    /// </summary>
    public interface IRendeles
    {
        Task<string> AddRendeles(int beszallitoID, DateTime datum, int felelosSzemely, List<Data.Tetel> rendeltTetelek);

        Task<string> ModifyRendeles(int rendelesID, int? beszallitoID = null, DateTime? datum = null, int? felelosSzemely = null, List<Data.Tetel> rendeltTetelek = null);

        Task<string> DeleteRendeles(int rendelesID);

        Task<List<Data.Rendeles>> GetRendeles(int? rendelesID = null);
    }
}
