﻿// <copyright file="TermekLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Repository;

    /// <summary>
    /// Termék logikáért felelős osztály
    /// </summary>
    public class TermekLogic : ParentLogic, ITermek
    {
        private ITermekRepo repo;
        private IBeszallitoTermekConnectorRepo connRepo;

        /// <summary>
        /// paraméter nélküli konstruktora
        /// </summary>
        public TermekLogic()
        {
            this.repo = new TermekRepo();
            this.connRepo = new BeszallitoTermekConnectorRepo();
        }

        /// <summary>
        /// Termék hozzáadása
        /// </summary>
        /// <param name="cikkszam">cikkszám, ez lesz az id</param>
        /// <param name="vonalkod">lesz egy vonalkod</param>
        /// <param name="termekNev">terméknek a neve</param>
        /// <param name="termekLeiras">termék leírása</param>
        /// <param name="ar">termék ára</param>
        /// <param name="mennyiseg">hány darab</param>
        /// <returns>válasz (sikeres-e)</returns>
        public async Task<string> AddTermek(int cikkszam, int vonalkod, string termekNev, string termekLeiras, int ar, int mennyiseg)
        {
            if (ar < 0)
            {
                throw new ArgumentException("Érvénytelen ár!");
            }

            if (!(await this.ErvenyesCikkszamE(cikkszam)))
            {
                throw new ArgumentException("Érvénytelen cikkszám!");
            }

            if (termekNev == null)
            {
                throw new ArgumentException("Érvénytelen név!");
            }

            try
            {
                await this.repo.Insert(new Termek() { Ar = ar, CikkSzam = cikkszam, Mennyiseg = mennyiseg, TermekLeiras = termekLeiras, TermekNev = termekNev, ToroltE = "N", VonalKod = vonalkod });
                return "Sikeresen hozzáadva!";
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }

        /// <summary>
        /// Termék törlése
        /// </summary>
        /// <param name="termekID">Termék id-ja szükséges hozzá (cikkszám)</param>
        /// <returns>válasz</returns>
        public async Task<string> DeleteTermek(int termekID)
        {
            try
            {
                await this.repo.ModifyTermek(termekID, toroltE: "Y");
                return "Sikeresen törölve!";
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }

        /// <summary>
        /// Érvényes e a cikkszám
        /// </summary>
        /// <param name="cikkszam">cikkszám megadása</param>
        /// <returns>igen vagy nem</returns>
        public async Task<bool> ErvenyesCikkszamE(int cikkszam)
        {
            try
            {
                if (cikkszam <= 0)
                {
                    return false;
                }

                return await this.repo.ErvenyesE(cikkszam);
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }

        /// <summary>
        /// Termék megkapása
        /// </summary>
        /// <param name="termekID">Termék cikkszáma kell hozzá</param>
        /// <returns>Termék vagy termékek visszakapása</returns>
        public async Task<List<Termek>> GetTermek(int? termekID = null)
        {
            try
            {
                if (termekID == null)
                {
                    return (await this.repo.GetAll()).Where(x => x.ToroltE == "N").ToList();
                }

                var t = await this.repo.GetById(termekID.Value);
                if (t != null && t.ToroltE == "N")
                {
                    return new List<Termek>() { t };
                }

                return null;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }

        /// <summary>
        /// Termék módosítása
        /// </summary>
        /// <param name="termekID">termék cikkszáma</param>
        /// <param name="vonalkod">termék vonalkódja</param>
        /// <param name="termekNev">termék neve</param>
        /// <param name="termekLeiras">termék leírása</param>
        /// <param name="ar">termék ára</param>
        /// <param name="mennyiseg">mennyisége</param>
        /// <param name="beszallitoId">beszállító id-je</param>
        /// <returns>válasz</returns>
        public async Task<string> ModifyTermek(int termekID, int? vonalkod = null, string termekNev = null, string termekLeiras = null, int? ar = null, int? mennyiseg = null, List<int> beszallitoId = null)
        {
            if (ar != null && ar.Value < 0)
            {
                throw new ArgumentException("Érvénytelen ár!");
            }

            try
            {
                await this.repo.ModifyTermek(termekID, vonalkod, termekNev, termekLeiras, ar, mennyiseg);
                if (beszallitoId != null)
                {
                    var old = await this.repo.GetById(termekID);
                    var oldConn = old.BeszallitoTermekConnector.ToList();
                    await this.connRepo.RemoveRange(oldConn);
                    List<BeszallitoTermekConnector> conn = new List<BeszallitoTermekConnector>();
                    foreach (var item in beszallitoId)
                    {
                        conn.Add(new BeszallitoTermekConnector() { BeszallitoId = item, Cikkszam = old.CikkSzam });
                    }

                    await this.connRepo.AddRange(conn);
                }

                return "Sikeresen módosítva";
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }
    }
}
