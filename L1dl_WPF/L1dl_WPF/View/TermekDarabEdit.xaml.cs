﻿// <copyright file="TermekDarabEdit.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;

    /// <summary>
    /// Interaction logic for TermekDarabEdit.xaml
    /// </summary>
    public partial class TermekDarabEdit : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="TermekDarabEdit"/> class.
        /// int tipusú paraméterű konstruktora
        /// </summary>
        /// <param name="i">integert vár</param>
        public TermekDarabEdit(int i)
        {
            this.InitializeComponent();
            this.tb_darab.Text = i.ToString();
        }

        /// <summary>
        /// Érték
        /// </summary>
        public string Value
        {
            get { return this.tb_darab.Text; }
        }

        /// <summary>
        /// Tesztez szükséges dolog
        /// </summary>
        public string TesztCucc
        {
            get { return this.tb_darab.Text; }
            set { this.tb_darab.Text = value; }
        }

        /// <summary>
        /// Darabról elveszített fókusz
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        public void tb_darab_LostFocus(object sender, RoutedEventArgs e)
        {
            try
            {
                int.Parse(this.tb_darab.Text);
            }
            catch
            {
                MessageBox.Show("EZ NEM SZÁM!");
                this.tb_darab.Text = "0";
            }
        }

        private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Close();
        }

        private void btnCANCEL_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
