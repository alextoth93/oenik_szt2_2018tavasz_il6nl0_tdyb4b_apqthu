﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using L1dl_WPF.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1dl_WPF.View.Tests
{
    [TestClass()]
    public class TermekDarabEditTests
    {
        [TestMethod()]
        public void tb_darab_LostFocusTest()
        {
            TermekDarabEdit t = new TermekDarabEdit(1);
            t.TesztCucc = "3";
            t.tb_darab_LostFocus(null,null);
            Assert.AreEqual("3", t.TesztCucc);

        }

        public void tb_darab_LostFocusTest_wrong()
        {
            TermekDarabEdit t = new TermekDarabEdit(1);
            t.TesztCucc = "sör";
            //t.tb_darab_LostFocus(null, null);    felugró ablak miatt nem
           // Assert.AreEqual("0", t.TesztCucc);

        }
    }
}