﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Tests
{
    [TestClass()]
    public class TermekLogicTests
    {
        [TestMethod()]
        public void ErvenyesCikkszamETest()
        {
            TermekLogic logic = new TermekLogic();
            Assert.AreEqual(logic.ErvenyesCikkszamE(100).Result, true);

        }

        [TestMethod()]
        public void ErvenyesCikkszamETest_invalid()
        {
            TermekLogic logic = new TermekLogic();
            Assert.AreEqual(logic.ErvenyesCikkszamE(10001).Result, false);
        }

        [TestMethod()]
        public void AddTermekTest()
        {
            TermekLogic logic = new TermekLogic();
            var r = logic.AddTermek(500, 100, "Teszt", "teszt", 20, 20).Result;
            Assert.AreEqual("Sikeresen hozzáadva!", r);
        }

        [TestMethod()]
        public void DeleteTermekTest()
        {
            TermekLogic logic = new TermekLogic();
            var r = logic.DeleteTermek(10000).Result;
            var d = logic.GetTermek().Result;
            Assert.AreEqual("Sikeresen törölve!", r);
        }

        [TestMethod()]
        public void GetTermekTest()
        {
            TermekLogic logic = new TermekLogic();
            var r = logic.GetTermek().Result;
            Assert.IsNotNull(r);
        }
        [TestMethod()]
        public void GetTermekTest_single()
        {
            TermekLogic logic = new TermekLogic();
            var r = logic.GetTermek(10001).Result;
            Assert.IsNotNull(r);
            Assert.AreEqual(r.Count,1);
        }

        [TestMethod()]
        public void ModifyTermekTest()
        {
            TermekLogic logic = new TermekLogic();
            var oldD = logic.GetTermek(10001).Result;
            var r = logic.ModifyTermek(10001,vonalkod:5321, termekNev:"sanyi").Result;
            var d = logic.GetTermek(10001).Result;
        }
    }
}