﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
//using Repository;

    /*
namespace BL
{
    public class FakeFelhasznaloLogic : IFelhasznalo
    {
        public string AddFelhasznalo(string Felhasznalonev, string Jelszo, int JogkorID)
        {
            return null;
        }

        public string ChangePassword(int FelhasznaloID, string OldJelszo, string NewJelszo)
        {
            return null;
        }

        public string DeleteUser(int FelhasznaloID)
        {
            f.Delete(FelhasznaloID);
            return null;
        }

        public Felhasznalo GetFelhasznalo(int FelhasznaloID)
        {
            return f.GetById(FelhasznaloID);
        }

        public Felhasznalo Login(string Felhasznalonev, string Jelszo)
        {
            //return f.GetById(int.Parse(Felhasznalonev));
            if (Felhasznalonev == "Bela")
            {
                var f = new Felhasznalo() { FelhasznaloNev = "Bela", Jelszo = "1234", FelhasznaloId = 42, JogkorId = 1, Tetel = null };
                var j = new Jogkor()
                {
                    JogkorNev = "GOD",
                    JogkorId = 1,
                    Felhasznalo = new Felhasznalo[] { f }
                };
                f.Jogkor = j;
                return f;
            }
            else
                throw new AccessViolationException("Nincs ilyen felhasználó!");
        }

        Repository.FelhasznaloRepository f = new Repository.FelhasznaloRepository();


        public string ModifyFelhasznalo(int FelhasznaloID, string Felhasznalonev = null, string Jelszo = null, int? JogkorID = null)
        {
            
            try
            {
                f.Modify(1);
                return "SIEKR";

            }
            catch (Exception e) 
            {

                return e.Message.ToString();
            }

        }
    }

    public class FakeJogkorLogic : IJogkor
    {
        public IEnumerable<Jogkor> GetJogkor(int? JogkorId = null)
        {
            return null;
        }
    }

    public class FakeRaktarLogic : IRaktar
    {
        public IEnumerable<Raktar> GetRaktar(int? RaktarID = null)
        {
            return null;
        }
    }

    public class FakeSzallitLogic : ISzallit
    {
        

        public string AddSzallit(int SzallitasID, int TetelID, List<Tetel> Tetelek)
        {
            return null;
        }

        public string DeleteSzallit(int SzallitID)
        {
            return null;
        }

        public IEnumerable<Szallit> GetSzallit(int? SzallitID = null)
        {
            return null;
        }

        public string ModifySzallit(int SzallitID, int? SzallitasID = null, int? TetelID = null)
        {
            return null;
        }
    }

    public class FakeSzallitasLogic : ISzallitas
    {
        public string AddSzallitas(DateTime Datum, int SzallitoID)
        {
            return null;
        }

        public string DeleteSzallitas(int SzallitasID)
        {
            return null;
        }

        public IEnumerable<Szallitas> GetSzallitas(int? SzallitasID = null)
        {
            return null;
        }

        public string ModifySzallitas(int SzallitasID, DateTime? Datum = null, int? SzallitoID = null)
        {
            return null;
        }
    }

    public class FakeSzallito : ISzallito
    {
        public string AddSzallito(string SzallitoNev)
        {
            return null;
        }

        public string DeleteSzallito(int SzallitoID)
        {
            return null;
        }

        public IEnumerable<Szallito> GetSzallito(int? SzallitoID = null)
        {
            return null;
        }

        public string ModifySzallito(int SzallitoID, string SzallitoNev)
        {
            return null;
        }
    }

    public class FakeTartalmaz : ITartalmaz
    {
        public string AddTartalmaz(int RaktarID, int Cikkszam, int Mennyiseg)
        {
            return null;
        }

        public string DeleteTartalmaz(int TartalmazID)
        {
            return null;
        }

        public IEnumerable<Tartalmaz> GetTartalmaz(int? TartalmazID = null)
        {
            return null;
        }

        public string ModifyTartalmaz(int TartalmazID, int? RaktarID = null, int? Cikkszam = null, int? Mennyiseg = null)
        {
            return null;
        }

        IEnumerable<Data.Tartalmaz> ITartalmaz.GetTartalmaz(int? TartalmazID)
        {
            return null;
        }
    }

    public class FakeTermek : ITermek
    {
        public string AddTermek(int Cikkszam, int Vonalkod, string Termeknev, string TermekLeiras, int Ar)
        {
            return null;
        }

        public bool CikkszamEllenorzo(int Cikkszam)
        {
            return null;
        }

        public string DeleteTermek(int TermekID)
        {
            return null;
        }

        public IEnumerable<Termek> GetTermek(int? TermekID = null)
        {
            return null;
        }

        public string ModifyTermek(int Cikkszam, int? Vonalkod = default(int?), string Termeknev = null, string TermekLeiras = null, int? Ar = default(int?))
        {
            return null;
        }

        public string ModifyTermek(int TermekID, int? Cikkszam = null, int? Vonalkod = null, string Termeknev = null, string TermekLeiras = null, int? Ar = null)
        {
            return null;
        }

        IEnumerable<Data.Termek> ITermek.GetTermek(int? TermekID)
        {
            return null;
        }
    }

    public class FakeTetel : ITetel
    {
        public string AddTetel(int Cikkszam, int FelhasznaloID, int RaktarID, int Mennyiseg)
        {
            return null;
        }

        public string DeleteTetel(int TetelID)
        {
            return null;
        }

        public IEnumerable<Tetel> GetTetel(int? TetelID = null)
        {
            return null;
        }

        public string ModifyTetel(int TetelID, int? Cikkszam = null, int? FelhasznaloID = null, int? RaktarID = null, int? Mennyiseg = null)
        {
            return null;
        }

        IEnumerable<Data.Tetel> ITetel.GetTetel(int? TetelID)
        {
            return null;
        }
    }
}
*/