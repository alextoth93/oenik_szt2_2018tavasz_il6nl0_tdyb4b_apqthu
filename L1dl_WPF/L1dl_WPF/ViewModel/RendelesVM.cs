﻿// <copyright file="RendelesVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Windows.Controls;
    using BL;
    using Data;

    /// <summary>
    /// Rendelés viewmodelljéért felelős osztály
    /// </summary>
    public class RendelesVM : INotifyPropertyChanged
    {
        private IBeszallito beszallitoLogic = new BeszallitoLogic();
        private ITermek termekLogic = new TermekLogic();
        private HomeVM homeVM;

        private ComboBox beszallitoComboBox = new ComboBox();
        private ListBox beszallitoTermekeiListBox = new ListBox();
        private Button rendelesLeadasButton = new Button();
        private Button rendelesModositasButton = new Button();

        private List<dynamic> comboboxListaja = new List<dynamic>();
        private List<dynamic> listboxListaja = new List<dynamic>();
        private List<dynamic> tetelLista = new List<dynamic>();

        private DataGrid dg = new DataGrid();
        private object selectedDatagridItem;
        private object selectedListBoxItem;

        private List<dynamic> list = new List<dynamic>();
        private string eventRoute;

        private bool comboboxIsEnabled;
        private object comboboxSelectedItem;

        private bool hozzaadIsEnabled = false;
        private bool modositIsEnabled = false;

        private int rendelesID;
        private Felhasznalo felh;

        /// <summary>
        /// Initializes a new instance of the <see cref="RendelesVM"/> class.
        /// 5 paraméterből álló konstruktora
        /// </summary>
        /// <param name="list">dinamikus lista</param>
        /// <param name="eventRoute">event route(string)</param>
        /// <param name="rendelesID">Rendelés id-ja</param>
        /// <param name="f">felhasználó</param>
        /// <param name="vm">home vmje</param>
        public RendelesVM(List<dynamic> list, string eventRoute, int rendelesID = -1, Data.Felhasznalo f = null, HomeVM vm = null)
        {
            if (vm != null)
            {
                this.homeVM = vm;
            }
            else
            {
                this.homeVM = new HomeVM();
            }

            this.EventRoute = eventRoute;
            this.List = list;
            this.ComboboxIsEnabled = true;
            if (rendelesID > -1)
            {
                this.rendelesID = rendelesID;
            }

            if (f != null)
            {
                this.Felh = f;
            }

            foreach (var item in this.homeVM?.Beszallitok)
            {
                this.comboboxListaja.Add(new { item.BeszállítóID,  item.Beszállítónév });
            }

            if (eventRoute == "uj")
            {
                ITetel logic = new TetelLogic();
                this.modositIsEnabled = false;
                if (this.TetelLista != null)
                {
                    this.hozzaadIsEnabled = true;
                }
            }
            else
            {
                ITetel logic = new TetelLogic();
                this.hozzaadIsEnabled = false;

                this.modositIsEnabled = true;
                foreach (var item in list.ElementAt(0).OlyanMintCsakNem)
                {
                    this.tetelLista.Add(new { Cikkszám = item.Cikkszam, Mennyiség = item.Mennyiseg, Terméknév = item.Termek.TermekNev });
                }

                this.ComboboxSelectedItem = this.comboboxListaja.SingleOrDefault(y => y.BeszállítóID == list.ElementAt(0).BeszállítóID);
                this.ComboboxIsEnabled = false;
            }
        }

        /// <summary>
        /// Property változása esetén fellépő eventhandler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Datagrid
        /// </summary>
        public DataGrid Dg
        {
            get
            {
                return this.dg;
            }

            set
            {
                this.dg = value;
                this.OnPropertyChanged("Dg");
            }
        }

        /// <summary>
        /// Kiválasztott Datagrid elemje
        /// </summary>
        public object SelectedDatagridItem
        {
            get
            {
                return this.selectedDatagridItem;
            }

            set
            {
                this.selectedDatagridItem = value;
                this.OnPropertyChanged("SelectedDatagridItem");
            }
        }

        /// <summary>
        /// Dinamikus lista - Combobox listája
        /// </summary>
        public List<dynamic> ComboboxListaja
        {
            get
            {
                return this.comboboxListaja;
            }

            set
            {
                this.comboboxListaja = value;
                this.OnPropertyChanged("ComboboxListaja");
            }
        }

        /// <summary>
        /// Beszallito Comboboxa
        /// </summary>
        public ComboBox BeszallitoComboBox
        {
            get
            {
                return this.beszallitoComboBox;
            }

            set
            {
                this.beszallitoComboBox = value;
                this.OnPropertyChanged("BeszallitoComboBox");
            }
        }

        /// <summary>
        /// Beszállító termék listboxa
        /// </summary>
        public ListBox BeszallitoTermekeiListBox
        {
            get
            {
                return this.beszallitoTermekeiListBox;
            }

            set
            {
                this.beszallitoTermekeiListBox = value;
                this.OnPropertyChanged("BeszallitoTermekeiListBox");
            }
        }

        /// <summary>
        /// Listbox listája
        /// </summary>
        public List<dynamic> ListboxListaja
        {
            get
            {
                return this.listboxListaja;
            }

            set
            {
                this.listboxListaja = value;
                this.OnPropertyChanged("ListboxListaja");
            }
        }

        /// <summary>
        /// Dinamikus tétellista
        /// </summary>
        public List<dynamic> TetelLista
        {
            get
            {
                return this.tetelLista;
            }

            set
            {
                this.tetelLista = value;
                this.OnPropertyChanged("TetelLista");
            }
        }

        /// <summary>
        /// Kiválasztott listabox elem
        /// </summary>
        public object SelectedListBoxItem
        {
            get
            {
                return this.selectedListBoxItem;
            }

            set
            {
                this.selectedListBoxItem = value;
                this.OnPropertyChanged("SelectedListBoxItem");
            }
        }

        /// <summary>
        /// Dinamikus lista
        /// </summary>
        public List<dynamic> List
        {
            get
            {
                return this.list;
            }

            set
            {
                this.list = value;
                this.OnPropertyChanged("List");
            }
        }

        /// <summary>
        /// Event route
        /// </summary>
        public string EventRoute
        {
            get
            {
                return this.eventRoute;
            }

            set
            {
                this.eventRoute = value;
                this.OnPropertyChanged("EventRoute");
            }
        }

        /// <summary>
        /// Combobox elérhető e
        /// </summary>
        public bool ComboboxIsEnabled
        {
            get
            {
                return this.comboboxIsEnabled;
            }

            set
            {
                this.comboboxIsEnabled = value;
                this.OnPropertyChanged("ComboboxIsEnabled");
            }
        }

        /// <summary>
        /// Combobox kiválasztott eleme
        /// </summary>
        public object ComboboxSelectedItem
        {
            get
            {
                return this.comboboxSelectedItem;
            }

            set
            {
                this.comboboxSelectedItem = value;
                this.OnPropertyChanged("ComboboxSelectedItem");
            }
        }

        /// <summary>
        /// Modosít elérhető e
        /// </summary>
        public bool ModositIsEnabled
        {
            get
            {
                return this.modositIsEnabled;
            }

            set
            {
                this.modositIsEnabled = value;
                this.OnPropertyChanged("ModositIsEnabled");
            }
        }

        /// <summary>
        /// Hozzaad elérhető e
        /// </summary>
        public bool HozzaadIsEnabled
        {
            get
            {
                return this.hozzaadIsEnabled;
            }

            set
            {
                this.hozzaadIsEnabled = value;
                this.OnPropertyChanged("HozzaadIsEnabled");
            }
        }

        /// <summary>
        /// Felhaználó
        /// </summary>
        public Felhasznalo Felh
        {
            get
            {
                return this.felh;
            }

            set
            {
                this.felh = value;
            }
        }

        /// <summary>
        /// Beszállító termék lista feltöltése
        /// </summary>
        /// <param name="beszallitoid">beszállító id-je</param>
        public async void BeszallitoTermekListFeltolt(int beszallitoid)
        {
            var full = await this.termekLogic.GetTermek();
            List<Data.Termek> r = new List<Termek>();
            this.beszallitoTermekeiListBox.Items.Clear();
            foreach (var item in full)
            {
                foreach (var item2 in item.BeszallitoTermekConnector)
                {
                    if (item2.BeszallitoId == beszallitoid)
                    {
                        r.Add(item);

                        this.beszallitoTermekeiListBox.Items.Add(new { item.CikkSzam, item.TermekNev });
                    }
                }
            }
        }

        /// <summary>
        /// Tetel Létrehoz
        /// </summary>
        /// <param name="cikkszam">cikkzám</param>
        /// <param name="termeknev">termék neve</param>
        /// <param name="mennyiseg">mennyiség</param>
        public void TetelLetrehoz(int cikkszam, string termeknev, int mennyiseg)
        {
            Tetel tetel = new Tetel();
            tetel.Cikkszam = cikkszam;
            tetel.Mennyiseg = mennyiseg;
            this.tetelLista.Add(new { Cikkszám = cikkszam, Terméknév = termeknev, Mennyiség = mennyiseg });
        }

        /// <summary>
        /// Módosító gomb klikk eseménye
        /// </summary>
        public async void ModifyButtonClick()
        {
            IRendeles logic = new RendelesLogic();

            int beszallitoID = ((dynamic)this.ComboboxSelectedItem).BeszállítóID;
            int felelosID = this.Felh.FelhasznaloId;
            List<Data.Tetel> r = new List<Tetel>();
            foreach (var item in this.tetelLista)
            {
                r.Add(new Tetel() { Cikkszam = item.Cikkszám, Mennyiseg = item.Mennyiség, RendelesId = this.rendelesID });
            }

            var d = await logic.ModifyRendeles(this.rendelesID, beszallitoID, DateTime.Now, felelosID, r);
        }

        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
