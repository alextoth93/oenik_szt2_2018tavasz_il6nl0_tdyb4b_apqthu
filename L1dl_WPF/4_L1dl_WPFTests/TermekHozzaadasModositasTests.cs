﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using L1dl_WPF.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1dl_WPF.View.Tests
{
    [TestClass()]
    public class TermekHozzaadasModositasTests
    {
        [TestMethod()]
        public void tb_termekCikkszam_LostFocusTest()
        {
            TermekHozzaadasModositas t = new TermekHozzaadasModositas(new ViewModel.TermekHozzaadasModositasVM(null, "uj"));

            t.TesztCucc = "78946";
            t.tb_termekCikkszam_LostFocus(null, null);
            Assert.AreNotEqual(string.Empty, t.TesztCucc);
        }
    }
}