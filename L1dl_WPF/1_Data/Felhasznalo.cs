namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Felhasznalo")]
    public partial class Felhasznalo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Felhasznalo()
        {
            Rendeles = new HashSet<Rendeles>();
        }

        public int FelhasznaloId { get; set; }

        [Required]
        [StringLength(255)]
        public string FelhasznaloNev { get; set; }

        [Required]
        [StringLength(255)]
        public string Jelszo { get; set; }

        public int JogkorId { get; set; }

        public DateTime? UtolsoBelepes { get; set; }

        [Required]
        [StringLength(1)]
        public string ToroltE { get; set; }

        public virtual Jogkor Jogkor { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Rendeles> Rendeles { get; set; }

        [Required]
        public int UtolsoAblak { get; set; }
    }
}
