﻿// <copyright file="IBeszallitoTermekConnectorRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface IBeszallitoTermekConnectorRepo : IGenericRepository<Data.BeszallitoTermekConnector>
    {
        Task AddRange(IEnumerable<Data.BeszallitoTermekConnector> input);

        Task RemoveRange(IEnumerable<Data.BeszallitoTermekConnector> input);
    }
}
