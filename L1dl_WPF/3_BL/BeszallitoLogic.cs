﻿// <copyright file="BeszallitoLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Repository;

    /// <summary>
    /// Beszállító Logikáért felelő osztály
    /// </summary>
    public class BeszallitoLogic : ParentLogic, IBeszallito
    {
        private IBeszallitoRepo repo;
        private IBeszallitoTermekConnectorRepo connRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BeszallitoLogic"/> class.
        /// paraméter nélküli konstruktora
        /// </summary>
        public BeszallitoLogic()
        {
            this.repo = new BeszallitoRepo();
            this.connRepo = new BeszallitoTermekConnectorRepo();
        }

        /// <summary>
        /// Beszállító hozzáadása
        /// </summary>
        /// <param name="nev">Neve</param>
        /// <param name="szallitottTermekek">Termékek</param>
        /// <returns>Válasz</returns>
        public async Task<string> AddBeszallito(string nev, List<Termek> szallitottTermekek)
        {
            try
            {
                Beszallito beszallito = new Beszallito() { BeszallitoNev = nev, ToroltE = "N" };
                List<BeszallitoTermekConnector> conn = new List<BeszallitoTermekConnector>();
                foreach (var item in szallitottTermekek)
                {
                    conn.Add(new BeszallitoTermekConnector() { Beszallito = beszallito, Cikkszam = item.CikkSzam });
                }

          // await this.repo.Insert(beszallito);
                await this.connRepo.AddRange(conn);
                return "Sikeres hozzáadás!";
            }
            catch (Exception)
            {
                return "Ismeretlen hiba történt!";
            }
        }

        /// <summary>
        /// Beszállító törlése
        /// </summary>
        /// <param name="beszallitoID">Kell a beszállító id-je</param>
        /// <returns>Folyamat válasza</returns>
        public async Task<string> DeleteBeszallito(int beszallitoID)
        {
            try
            {
                var old = await this.repo.GetById(beszallitoID);
                if (old.ToroltE == "Y")
                {
                    return "Sikeres törlés";
                }

                await this.repo.ModifyBeszallito(old.BeszallitoId, isDeleted: "Y");
                return "Sikeres törlés";
            }
            catch (Exception)
            {
                return "Ismeretlen hiba történt!";
            }
        }

        /// <summary>
        /// Beszállító megkapása
        /// </summary>
        /// <param name="beszallitoID">beszálllító id-je kell (int)</param>
        /// <returns>egy lista amiben a beszálllító vagy beszállítók vannak</returns>
        public async Task<List<Beszallito>> GetBeszallito(int? beszallitoID = null)
        {
            if (beszallitoID == null)
            {
                return (await this.repo.GetAll()).Where(x => x.ToroltE == "N").ToList();
            }

            var b = await this.repo.GetById(beszallitoID.Value);
            if (b.ToroltE == "N")
            {
                return new List<Beszallito>() { b };
            }

            return null;
        }

        /// <summary>
        /// Beszállító módosítása
        /// </summary>
        /// <param name="beszallitoID">beszállító id-ja</param>
        /// <param name="nev">beszállító neve</param>
        /// <param name="szallitottTermekek">szállított termékek</param>
        /// <returns>válasz</returns>
        public async Task<string> ModifyBeszallito(int beszallitoID, string nev = null, List<Termek> szallitottTermekek = null)
        {
            try
            {
                var old = await this.repo.GetById(beszallitoID);
                if (old == null)
                {
                    throw new ArgumentException("Nincs ilyen beszállító!");
                }

                await this.repo.ModifyBeszallito(beszallitoID, nev);
                if (szallitottTermekek != null)
                {
                    var oldConn = old.BeszallitoTermekConnector.ToList();
                    await this.connRepo.RemoveRange(oldConn);
                    List<BeszallitoTermekConnector> conn = new List<BeszallitoTermekConnector>();
                    foreach (var item in szallitottTermekek)
                    {
                        conn.Add(new BeszallitoTermekConnector() { BeszallitoId = old.BeszallitoId, Cikkszam = item.CikkSzam });
                    }

                    await this.connRepo.AddRange(conn);
                }

                return "Sikeres módosítás";
            }
            catch (Exception)
            {
                return "Ismeretlen hiba történt!";
            }
        }
    }
}
