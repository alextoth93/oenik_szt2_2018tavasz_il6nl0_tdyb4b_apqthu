﻿// <copyright file="HomeVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using BL;
    using Data;
    using L1dl_WPF.View;
    using L1dl_WPF.ViewModel;

    /// <summary>
    /// HomeVM-ért felelős osztály
    /// </summary>
    public class HomeVM : INotifyPropertyChanged
    {
        private static Felhasznalo aktualis;
        private List<dynamic> termekek;
        private List<dynamic> beszallitok;
        private List<dynamic> rendelesek;
        private TabControl tc = new TabControl();
        private Button ujBtn = new Button();
        private Button modositBtn = new Button();
        private Button torolBtn = new Button();
        private DataGrid dg;
        private bool torolIsEnabled;
        private object selectedDatagridItem;
        private object selectedBeszallitoDatagridItem;
        private object selectedRendelesDatagridItem;
        private Data.Model lidl = new Model();
        private List<dynamic> termekListForBinding;
        private List<dynamic> beszallitoListForBinding;
        private List<dynamic> rendelesListForBinding;

        /// <summary>
        /// Initializes a new instance of the <see cref="HomeVM"/> class.
        /// paraméter nélküli konstruktora
        /// </summary>
        public HomeVM()
        {
            this.beszallitok = new List<dynamic>();
            this.termekek = new List<dynamic>();
            this.rendelesek = new List<dynamic>();

            this.UjBtn1.Content = "Termék felvétele";
            this.modositBtn.Content = "Termék módosítása";
            this.torolBtn.Content = "Termék törlése";
            this.torolIsEnabled = true;
            this.TermekListForBinding = new List<dynamic>();
            this.BeszallitoListForBinding = new List<dynamic>();
            this.RendelesListForBinding = new List<dynamic>();
            Task.Run(() => this.Listazas()).Wait();
        }

        /// <summary>
        /// Property megváltozása esetén történ eventhandler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Aktuális felhasználó
        /// </summary>
        public static Felhasznalo Aktualis
        {
            get
            {
                return aktualis;
            }

            set
            {
                aktualis = value;
            }
        }

        /// <summary>
        /// Termékek
        /// </summary>
        public List<dynamic> Termekek
        {
            get
            {
                return this.termekek;
            }

            set
            {
                this.termekek = value;
                this.OnPropertyChanged("Termekek");
            }
        }

        /// <summary>
        /// TabControl
        /// </summary>
        public TabControl Tc
        {
            get
            {
                return this.tc;
            }

            set
            {
                this.tc = value;
                this.OnPropertyChanged("Tc");
            }
        }

        /// <summary>
        /// Új gomb
        /// </summary>
        public Button UjBtn
        {
            get
            {
                return this.UjBtn1;
            }

            set
            {
                this.UjBtn1 = value;
                this.OnPropertyChanged("UjBtn");
            }
        }

        /// <summary>
        /// Módosító gomb
        /// </summary>
        public Button ModositBtn
        {
            get
            {
                return this.modositBtn;
            }

            set
            {
                this.modositBtn = value;
                this.OnPropertyChanged("ModositBtn");
            }
        }

        /// <summary>
        /// Töröl gomb
        /// </summary>
        public Button TorolBtn
        {
            get
            {
                return this.torolBtn;
            }

            set
            {
                this.torolBtn = value;
                this.OnPropertyChanged("TorolBtn");
            }
        }

        /// <summary>
        /// Törlés elérhető e
        /// </summary>
        public bool TorolIsEnabled
        {
            get
            {
                return this.torolIsEnabled;
            }

            set
            {
                this.torolIsEnabled = value;
                this.OnPropertyChanged("TorolIsEnabled");
            }
        }

        /// <summary>
        /// Kiválasztott Datagrid elem
        /// </summary>
        public object SelectedDatagridItem
        {
            get
            {
                return this.selectedDatagridItem;
            }

            set
            {
                this.selectedDatagridItem = value;
                this.OnPropertyChanged("SelectedDatagridItem");
            }
        }

        /// <summary>
        /// Datagrid
        /// </summary>
        public DataGrid Dg
        {
            get
            {
                return this.dg;
            }

            set
            {
                this.dg = value;
                this.OnPropertyChanged("Dg");
            }
        }

        /// <summary>
        /// Terméklista kötéshez
        /// </summary>
        public List<dynamic> TermekList_forBinding
        {
            get
            {
                return this.TermekListForBinding;
            }

            set
            {
                this.TermekListForBinding = value;
                this.OnPropertyChanged("TermekList_forBinding");
            }
        }

        /// <summary>
        /// Lidl model
        /// </summary>
        public Data.Model Lidl
        {
            get
            {
                return this.lidl;
            }

            set
            {
                this.lidl = value;
                this.OnPropertyChanged("Lidl");
            }
        }

        /// <summary>
        /// Beszállító lita kötéshez
        /// </summary>
        public List<dynamic> BeszallitoList_forBinding
        {
            get
            {
                return this.BeszallitoListForBinding;
            }

            set
            {
                this.BeszallitoListForBinding = value;
                this.OnPropertyChanged("BeszallitoList_forBinding");
            }
        }

        /// <summary>
        /// Beszállítók
        /// </summary>
        public List<dynamic> Beszallitok
        {
            get
            {
                return this.beszallitok;
            }

            set
            {
                this.beszallitok = value;
                this.OnPropertyChanged("Beszallitok");
            }
        }

        /// <summary>
        /// Rendelés lista kötéshez
        /// </summary>
        public List<dynamic> RendelesList_forBinding
        {
            get
            {
                return this.RendelesListForBinding;
            }

            set
            {
                this.RendelesListForBinding = value;
                this.OnPropertyChanged("RendelesList_forBinding");
            }
        }

        /// <summary>
        /// Rendelések
        /// </summary>
        public List<dynamic> Rendelesek
        {
            get
            {
                return this.rendelesek;
            }

            set
            {
                this.rendelesek = value;
                this.OnPropertyChanged("Rendelesek");
            }
        }

        /// <summary>
        /// Kiválasztott Rendelés Datagrid elem
        /// </summary>
        public object SelectedRendelesDatagridItem
        {
            get
            {
                return this.selectedRendelesDatagridItem;
            }

            set
            {
                this.selectedRendelesDatagridItem = value;
                this.OnPropertyChanged("SelectedRendelesDatagridItem");
            }
        }

        /// <summary>
        /// Kiválasztott beszállító datagrid elem
        /// </summary>
        public object SelectedBeszallitoDatagridItem
        {
            get
            {
                return this.selectedBeszallitoDatagridItem;
            }

            set
            {
                this.selectedBeszallitoDatagridItem = value;
                this.OnPropertyChanged("SelectedBeszallitoDatagridItem");
            }
        }

        /// <summary>
        /// Új gomb
        /// </summary>
        public Button UjBtn1
        {
            get
            {
                return this.ujBtn;
            }

            set
            {
                this.ujBtn = value;
            }
        }

        /// <summary>
        /// Termék lista kötéshez
        /// </summary>
        public List<dynamic> TermekListForBinding
        {
            get
            {
                return this.termekListForBinding;
            }

            set
            {
                this.termekListForBinding = value;
                this.OnPropertyChanged("TermekListForBinding");
            }
        }

        /// <summary>
        /// Beszállító lista kötéshez
        /// </summary>
        public List<dynamic> BeszallitoListForBinding
        {
            get
            {
                return this.beszallitoListForBinding;
            }

            set
            {
                this.beszallitoListForBinding = value;
                this.OnPropertyChanged("BeszallitoListForBinding");
            }
        }

        /// <summary>
        /// Rendelés lista kötéshez
        /// </summary>
        public List<dynamic> RendelesListForBinding
        {
            get
            {
                return this.rendelesListForBinding;
            }

            set
            {
                this.rendelesListForBinding = value;
                this.OnPropertyChanged("RendelesListForBinding");
            }
        }

        /// <summary>
        /// Listázás
        /// </summary>
        /// <returns>Task</returns>
        public async Task Listazas()
        {
            ITermek termekLogic = new TermekLogic();
            var d = termekLogic;
            var t = await termekLogic.GetTermek();
            foreach (var item in t)
            {
                this.Termekek.Add(new { Cikkszám = item.CikkSzam, Terméknév = item.TermekNev, Termék_leírás = item.TermekLeiras, Ár = item.Ar, Vonalkód = item.VonalKod, Mennyiség = item.Mennyiseg });
            }

            IBeszallito beszallitoLogic = new BeszallitoLogic();
            var b = await beszallitoLogic.GetBeszallito();
            foreach (var item in b)
            {
                this.Beszallitok.Add(new { BeszállítóID = item.BeszallitoId, Beszállítónév = item.BeszallitoNev, Termékei = item.BeszallitoTermekConnector, Termékek = item.BeszallitoTermekConnector.Count + " darab" });
            }

            IRendeles rendelesLogic = new RendelesLogic();
            var r = await rendelesLogic.GetRendeles();
            foreach (var item in r)
            {
                this.Rendelesek.Add(new
                {
                    RendelésID = item.RendelesId,
                    Dátum = item.Datum.ToString("yyyy.MM.dd"),
                    Felelős = item.Felhasznalo.FelhasznaloNev,
                    BeszállítóID = item.BeszallitoId,
                    OlyanMintCsakNem = item.Tetel.ToList()
                });
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
