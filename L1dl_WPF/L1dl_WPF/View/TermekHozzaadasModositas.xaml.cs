﻿// <copyright file="TermekHozzaadasModositas.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BL;
    using L1dl_WPF.ViewModel;

    /// <summary>
    /// Interaction logic for TermekHozzaadasModositas.xaml
    /// </summary>
    public partial class TermekHozzaadasModositas : Window
    {
        private RendelesVM rendelesVM;
        private HomeVM homeVM;
        private BL.ITermek tl = new TermekLogic();
        private TermekHozzaadasModositasVM thmVM;

        /// <summary>
        /// Initializes a new instance of the <see cref="TermekHozzaadasModositas"/> class.
        /// TermekHozzaadasModositasVM viewmodell paraméterű konstruktor
        /// </summary>
        /// <param name="viewmodel">TermekHozzaadasModositasVM viewmodel</param>
        public TermekHozzaadasModositas(TermekHozzaadasModositasVM viewmodel)
        {
            this.InitializeComponent();
            this.thmVM = viewmodel;
            this.DataContext = viewmodel;
        }

        /// <summary>
        /// Tesztelés segéd
        /// </summary>
        public string TesztCucc
        {
            get { return this.tb_termekCikkszam.Text; }
            set { this.tb_termekCikkszam.Text = value; }
        }

        /// <summary>
        /// Termeék cikkszámáról elveszített fókusz
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        public async void tb_termekCikkszam_LostFocus(object sender, RoutedEventArgs e)
        {
            bool l = false;
            try
            {
                l = await this.tl.ErvenyesCikkszamE(int.Parse(this.tb_termekCikkszam.Text));
            }
            catch (Exception)
            {
            }

            if (!l)
            {
                MessageBox.Show("Érvénytelen cikkszám!");
                this.tb_termekCikkszam.Text = string.Empty;
            }
        }

        private async void btn_modosit_Click(object sender, RoutedEventArgs e)
        {
            await this.tl.ModifyTermek(int.Parse(this.tb_termekCikkszam.Text), int.Parse(this.tb_termekVonalkod.Text), this.tb_termekNev.Text, this.tb_termekLeiras.Text, int.Parse(this.tb_termekAr.Text), int.Parse(this.tb_termekDarab.Text));

            this.homeVM = new HomeVM();
            Window home = new View.Home();

            this.Close();
            home.ShowDialog();
        }

        private void btn_hozzaad_Click(object sender, RoutedEventArgs e)
        {
            this.homeVM = new HomeVM();
            this.rendelesVM = new RendelesVM(this.homeVM.RendelesList_forBinding, "uj");
            Rendeles rendelesWindow = new Rendeles(this.rendelesVM);

            rendelesWindow.ShowDialog();
        }

        private async void btn_dbhozzaad_Click(object sender, RoutedEventArgs e)
        {
            await this.tl.AddTermek(int.Parse(this.tb_termekCikkszam.Text), int.Parse(this.tb_termekVonalkod.Text), this.tb_termekNev.Text, this.tb_termekLeiras.Text, int.Parse(this.tb_termekAr.Text), int.Parse(this.tb_termekDarab.Text));

            this.homeVM = new HomeVM();
            Window home = new View.Home();

            this.Close();
            home.ShowDialog();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
        }

        private void tb_termekNev_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.thmVM.Route == "modosit")
            {
                this.btn_modosit.IsEnabled = true;
            }

            if (this.thmVM.Route == "uj")
            {
                if (this.tb_termekNev.Text != string.Empty && this.tb_termekLeiras.Text != string.Empty && this.tb_termekDarab.Text != string.Empty &&
                    this.tb_termekCikkszam.Text != string.Empty && this.tb_termekAr.Text != string.Empty)
                {
                    this.btn_hozzaad.IsEnabled = true;
                }
            }
        }

        private void tb_termekVonalkod_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void btn_cancel_Click(object sender, RoutedEventArgs e)
        {
            Home h = new Home();
            this.Close();
            h.ShowDialog();
        }
    }
}
