﻿// <copyright file="FelhasznaloRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

   public class FelhasznaloRepo : GenericRepository<Felhasznalo>, IFelhasznaloRepo
    {
        public async override Task<Felhasznalo> GetById(int id)
        {
            return await Task.Factory.StartNew(() => { return (from q in this.Context.Felhasznalo where q.FelhasznaloId == id select q).SingleOrDefault(); });
        }

        public async Task<Felhasznalo> GetByNevAndJelszo(string nev, string passw)
        {
            return await Task.Factory.StartNew(() =>
            {
               return (from q in this.Context.Felhasznalo where q.FelhasznaloNev == nev && q.Jelszo == passw select q).SingleOrDefault();
            });
        }

        public async Task ModifyFelhasznalo(int felhasznaloID, string nev = null, string jelszo = null, int? jogkorID = null, DateTime? utolsoBelepes = null, int? utolsoAblak = null)
        {
            var old = await this.GetById(felhasznaloID);
            if (old == null)
            {
                throw new ArgumentException("Nincs ilyen felhasználó!");
            }

            if (nev != null)
            {
                old.FelhasznaloNev = nev;
            }

            if (jelszo != null)
            {
                old.Jelszo = jelszo;
            }

            if (jogkorID != null)
            {
                old.JogkorId = jogkorID.Value;
            }

            if (utolsoBelepes != null)
            {
                old.UtolsoBelepes = utolsoBelepes.Value;
            }

            if (utolsoAblak != null)
            {
                old.UtolsoAblak = utolsoAblak.Value;
            }

            await this.Context.SaveChangesAsync();
        }
    }
}
