﻿// <copyright file="ITetelRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface ITetelRepo : IGenericRepository<Data.Tetel>
    {
        Task ModifyTetel(int tetelId, int? rendelesId = null, int? cikkszam = null, int? mennyiseg = null);
    }
}
