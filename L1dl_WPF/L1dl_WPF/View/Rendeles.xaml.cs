﻿// <copyright file="Rendeles.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF.View
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using BL;
    using Data;
    using L1dl_WPF.ViewModel;

    /// <summary>
    /// Interaction logic for Rendeles.xaml
    /// </summary>
    public partial class Rendeles : Window
    {
        private HomeVM homeVM = new HomeVM();
        private RendelesVM rendelesVM;

        /// <summary>
        /// Initializes a new instance of the <see cref="Rendeles"/> class.
        /// Rendeles View Modell paraméterű konstruktora
        /// </summary>
        /// <param name="viewmodel">Rendelés Vm</param>
        public Rendeles(RendelesVM viewmodel)
        {
            this.InitializeComponent();
            this.rendelesVM = viewmodel;
            if (!this.rendelesVM.ComboboxIsEnabled)
            {
                this.btn_termekTetelbe.Visibility = Visibility.Collapsed;
                this.btn_tetelbolKi.Visibility = Visibility.Collapsed;
                this.btn_rendelesLeadas.Visibility = Visibility.Collapsed;
                this.lb_termekek.IsEnabled = false;
                this.dg_tetelek.IsReadOnly = true;
                this.cancel.Content = "Vissza";
            }

            this.DataContext = viewmodel;
        }

        /// <summary>
        /// Tesztbox
        /// </summary>
        public ComboBox TesztBox
        {
            get { return this.cb_beszallito; }
        }

        /// <summary>
        /// Tiltás
        /// </summary>
        public void Tilto()
        {
            this.cb_beszallito.IsEnabled = false;
        }

        /// <summary>
        /// Engedélyező
        /// </summary>
        public void Engedelyezo()
        {
            this.cb_beszallito.IsEnabled = true;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Window home = new View.Home();
            home.ShowDialog();
        }

        private void btn_termekTetelbe_Click(object sender, RoutedEventArgs e)
        {
            TermekDarabEdit ize = new TermekDarabEdit(0);
            string s = "0";
            if (ize.ShowDialog() == true)
            {
                s = ize.Value;

                this.rendelesVM.TetelLetrehoz(
                    ((dynamic)this.rendelesVM.SelectedListBoxItem).CikkSzam, ((dynamic)this.rendelesVM.SelectedListBoxItem).TermekNev, int.Parse(s));

                this.dg_tetelek.ItemsSource = null;
                this.dg_tetelek.ItemsSource = this.rendelesVM.TetelLista;
                foreach (var item in this.dg_tetelek.Columns)
                {
                    if (item.SortMemberPath == "Mennyiség")
                    {
                        item.IsReadOnly = false;
                    }
                }

                this.InvalidateVisual();
            }
        }

        private void btn_tetelbolKi_Click(object sender, RoutedEventArgs e)
        {
            var old = (dynamic)this.dg_tetelek.SelectedItem;
            this.rendelesVM.TetelLista.Remove(old);
            this.dg_tetelek.ItemsSource = null;
            this.dg_tetelek.ItemsSource = this.rendelesVM.TetelLista;
        }

        private void cb_beszallito_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.rendelesVM.BeszallitoTermekListFeltolt(((dynamic)this.cb_beszallito.SelectedItem).BeszállítóID);
        }

        private void dg_tetelek_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.rendelesVM.ModositIsEnabled)
            {
                return;
            }

            if (this.dg_tetelek.SelectedItem == null)
            {
                return;
            }

            var old = (dynamic)this.dg_tetelek.SelectedItem;
            TermekDarabEdit ize = new TermekDarabEdit(old.Mennyiség);
            if (ize.ShowDialog() == true)
            {
                this.rendelesVM.TetelLista.Remove(old);
                dynamic newOld = new { old.Cikkszám, old.Terméknév, Mennyiség = int.Parse(ize.Value) };
                this.rendelesVM.TetelLista.Add(newOld);

                this.dg_tetelek.ItemsSource = null;
                this.dg_tetelek.ItemsSource = this.rendelesVM.TetelLista;
            }
        }

        private async void btn_rendelesLeadas_Click(object sender, RoutedEventArgs e)
        {
            IRendeles rendelesLogic = new RendelesLogic();
            IFelhasznalo felhLogic = new FelhasznaloLogic();

            int beszallitoID = ((dynamic)this.cb_beszallito.SelectedItem).BeszállítóID;
            DateTime datum = DateTime.Now;

            List<Tetel> r = new List<Tetel>();
            foreach (var item in (dynamic)this.rendelesVM?.TetelLista)
            {
                r.Add(new Tetel() { Cikkszam = item.Cikkszám, Mennyiseg = item.Mennyiség });
            }

            var d = await rendelesLogic.AddRendeles(beszallitoID, datum, this.rendelesVM.Felh.FelhasznaloId, r);

            this.Close();
        }

        private void btn_rendelesModositas_Click(object sender, RoutedEventArgs e)
        {
            this.rendelesVM.ModifyButtonClick();
        }

        private void cancel_Click(object sender, RoutedEventArgs e)
        {
            Home h = new Home();
            this.Close();
            h.ShowDialog();
        }

        private void lb_termekek_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.btn_termekTetelbe.IsEnabled = true;
        }

        private void dg_tetelek_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.dg_tetelek.Items.Count > 0)
            {
                this.Tilto();
            }
            else
            {
                this.Engedelyezo();
            }

            this.btn_tetelbolKi.IsEnabled = true;
        }
    }
}
