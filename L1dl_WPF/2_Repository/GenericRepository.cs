﻿// <copyright file="GenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

    public abstract class GenericRepository<TEntity>
        where TEntity : class
    {
        private Model context;

        public GenericRepository()
        {
            this.Context = new Model();
        }

        protected Model Context
        {
            get { return this.context; }
            set { this.context = value; }
        }

        public abstract Task<TEntity> GetById(int id);

        public async Task Delete(int id)
        {
            TEntity oldentity = await this.GetById(id);
            if (oldentity == null)
            {
                throw new ArgumentException("NO DATA");
            }

            await this.Delete(oldentity);
        }

        public async Task Delete(TEntity oldentity)
        {
            this.Context.Set<TEntity>().Remove(oldentity);
            this.Context.Entry<TEntity>(oldentity).State = EntityState.Deleted;
            await this.Context.SaveChangesAsync();
        }

        public void Dispose()
        {
            this.Context.Dispose();
        }

        public async Task<IQueryable<TEntity>> Get(Expression<Func<TEntity, bool>> condition)
        {
            var v = await this.GetAll();
            return v.Where(condition);
        }

        public async Task<IQueryable<TEntity>> GetAll()
        {
            return await Task.Factory.StartNew(() =>
            {
                return this.Context.Set<TEntity>();
            });
        }

        public async Task Insert(TEntity newentity)
        {
            this.Context.Set<TEntity>().Add(newentity);
            await this.Context.SaveChangesAsync();
        }
    }
}
