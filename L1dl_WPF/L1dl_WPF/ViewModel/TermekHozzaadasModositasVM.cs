﻿// <copyright file="TermekHozzaadasModositasVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Controls;

    /// <summary>
    /// Termék hozzáadása/módosítása Viewmodellért felelős osztály
    /// </summary>
    public class TermekHozzaadasModositasVM : INotifyPropertyChanged
    {
        private TextBox termeknevTB = new TextBox();
        private TextBox arTB = new TextBox();
        private TextBox mennyisegTB = new TextBox();
        private TextBox cikkszamTB = new TextBox();
        private TextBox vonalkodTB = new TextBox();
        private TextBox termekleirasTB = new TextBox();
        private ComboBox beszallitoCB = new ComboBox();
        private List<dynamic> list = new List<dynamic>();
        private bool hozzaadisActive = false;
        private bool modositisActive = false;
        private DataGrid dg = new DataGrid();
        private object selectedDatagridItem;
        private string route;

        private bool cikkszamIsEnabled;

        /// <summary>
        /// Initializes a new instance of the <see cref="TermekHozzaadasModositasVM"/> class.
        /// két paraméteres konstruktora
        /// </summary>
        /// <param name="list">dinamikus lista</param>
        /// <param name="eventRoute">eventRoute</param>
        public TermekHozzaadasModositasVM(List<dynamic> list, string eventRoute)
        {
            this.List = list;
            this.Route = eventRoute;

            if (this.Route == "modosit")
            {
                this.hozzaadisActive = false;
                var d = list.ElementAt(0);
                this.termeknevTB.Text = list.ElementAt(0).Terméknév;
                this.termekleirasTB.Text = list.ElementAt(0).Termék_leírás;
                this.arTB.Text = list.ElementAt(0).Ár.ToString();
                this.vonalkodTB.Text = list.ElementAt(0).Vonalkód.ToString();
                this.cikkszamTB.Text = list.ElementAt(0).Cikkszám.ToString();
                this.cikkszamTB.IsReadOnly = true;
                this.mennyisegTB.Text = list.ElementAt(0).Mennyiség.ToString();
                this.CikkszamIsEnabled = false;
            }
            else
            {
                this.modositisActive = false;
                this.termeknevTB.Text = string.Empty;
                this.termekleirasTB.Text = string.Empty;
                this.arTB.Text = string.Empty;
                this.vonalkodTB.Text = string.Empty;
                this.cikkszamTB.Text = string.Empty;
                this.CikkszamIsEnabled = true;
            }
        }

        /// <summary>
        /// Property változásához használt eventhandler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Terméknév textboxa
        /// </summary>
        public TextBox TermeknevTB
        {
            get
            {
                return this.termeknevTB;
            }

            set
            {
                this.termeknevTB = value;
                this.OnPropertyChanged("TermeknevTB");
            }
        }

        /// <summary>
        /// Ár textboxa
        /// </summary>
        public TextBox ArTB
        {
            get
            {
                return this.arTB;
            }

            set
            {
                this.arTB = value;
                this.OnPropertyChanged("ArTB");
            }
        }

        /// <summary>
        /// Mennyiség TextBoxa
        /// </summary>
        public TextBox MennyisegTB
        {
            get
            {
                return this.mennyisegTB;
            }

            set
            {
                this.mennyisegTB = value;
                this.OnPropertyChanged("MennyisegTB");
            }
        }

        /// <summary>
        /// Cikkszam TextBoxa
        /// </summary>
        public TextBox CikkszamTB
        {
            get
            {
                return this.cikkszamTB;
            }

            set
            {
                this.cikkszamTB = value;
                this.OnPropertyChanged("CikkszamTB");
            }
        }

        /// <summary>
        /// Vonalkod TextBoxa
        /// </summary>
        public TextBox VonalkodTB
        {
            get
            {
                return this.vonalkodTB;
            }

            set
            {
                this.vonalkodTB = value;
                this.OnPropertyChanged("VonalkodTB");
            }
        }

        /// <summary>
        /// Termekleiras TextBoxa
        /// </summary>
        public TextBox TermekleirasTB
        {
            get
            {
                return this.termekleirasTB;
            }

            set
            {
                this.termekleirasTB = value;
                this.OnPropertyChanged("TermekleirasTB");
            }
        }

        /// <summary>
        /// Beszallito ComboBoxa
        /// </summary>
        public ComboBox BeszallitoCB
        {
            get
            {
                return this.beszallitoCB;
            }

            set
            {
                this.beszallitoCB = value;
                this.OnPropertyChanged("BeszallitoCB");
            }
        }

        /// <summary>
        /// Dinamikus lista
        /// </summary>
        public List<dynamic> List
        {
            get
            {
                return this.list;
            }

            set
            {
                this.list = value;
                this.OnPropertyChanged("List");
            }
        }

        /// <summary>
        /// DatagGrid
        /// </summary>
        public DataGrid Dg
        {
            get
            {
                return this.dg;
            }

            set
            {
                this.dg = value;
                this.OnPropertyChanged("Dg");
            }
        }

        /// <summary>
        /// Kiválasztott Datagrid elem
        /// </summary>
        public object SelectedDatagridItem
        {
            get
            {
                return this.selectedDatagridItem;
            }

            set
            {
                this.selectedDatagridItem = value;
                this.OnPropertyChanged("SelectedDatagridItem");
            }
        }

        /// <summary>
        /// Route
        /// </summary>
        public string Route
        {
            get
            {
                return this.route;
            }

            set
            {
                this.route = value;
                this.OnPropertyChanged("Route");
            }
        }

        /// <summary>
        /// Hozzáadás Aktív e
        /// </summary>
        public bool HozzaadisActive
        {
            get
            {
                return this.hozzaadisActive;
            }

            set
            {
                this.hozzaadisActive = value;
                this.OnPropertyChanged("HozzaadisActive");
            }
        }

        /// <summary>
        /// Módostás aktív e
        /// </summary>
        public bool ModositisActive
        {
            get
            {
                return this.modositisActive;
            }

            set
            {
                this.modositisActive = value;
                this.OnPropertyChanged("ModositisActive");
            }
        }

        /// <summary>
        /// Cikkszám elérhető e
        /// </summary>
        public bool CikkszamIsEnabled
        {
            get
            {
                return this.cikkszamIsEnabled;
            }

            set
            {
                this.cikkszamIsEnabled = value;
                this.OnPropertyChanged("CikkszamIsEnabled");
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
