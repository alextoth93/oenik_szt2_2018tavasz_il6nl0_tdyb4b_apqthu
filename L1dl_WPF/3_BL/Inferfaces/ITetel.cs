﻿// <copyright file="ITetel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Tételekért felelős interface
    /// </summary>
    public interface ITetel
    {
        Task<string> ModifyTetel(int tetelID, int? cikkszam = null, int? mennyiseg = null);

        Task<string> DeleteTetel(int tetelID);

        Task<List<Data.Tetel>> GetTetel(int? tetelID = null);
    }
}
