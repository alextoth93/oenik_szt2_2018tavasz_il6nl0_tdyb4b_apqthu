﻿using NUnit.Framework;
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using L1dl_WPF.ViewModel;
using L1dl_WPF.View;
using BL;
using Data;
using System.Windows.Controls;

namespace _4_VMTests
{
    [TestClass()]
    public class BeszallitoHozzaadasModositasVMTests
    {
        [TestMethod()]
        public void Test()
        {
            List<dynamic> list = new List<dynamic>();
            Beszallito beszallito = new Beszallito();
            beszallito.BeszallitoId = 1;
            beszallito.BeszallitoNev = "Egyeske";
            list.Add(beszallito);
            string eventroute = "uj";
            BeszallitoHozzaadasModositasVM vm = new BeszallitoHozzaadasModositasVM(list, eventroute);

            BeszallitoHozzaadasModositas window = new BeszallitoHozzaadasModositas(vm);


            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual((vm.BeszallitoNevTextBox as TextBox).Text, "");
        }
    }
}
