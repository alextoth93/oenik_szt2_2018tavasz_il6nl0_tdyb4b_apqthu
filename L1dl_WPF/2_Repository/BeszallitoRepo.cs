﻿// <copyright file="BeszallitoRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;

   public class BeszallitoRepo : GenericRepository<Beszallito>, IBeszallitoRepo
    {
        public async override Task<Beszallito> GetById(int id)
        {
            return await Task.Factory.StartNew(() =>
            {
                return (from q in this.Context.Beszallito where q.BeszallitoId == id select q).SingleOrDefault();
            });
        }

        public async Task ModifyBeszallito(int beszallitoId, string nev = null, string isDeleted = null)
        {
            var old = await this.GetById(beszallitoId);
            if (old == null)
            {
                throw new ArgumentException("Nincs ilyen beszállító");
            }

            if (isDeleted != null)
            {
                old.ToroltE = isDeleted;
            }

            if (nev != null)
            {
                old.BeszallitoNev = nev;
            }

            await this.Context.SaveChangesAsync();
        }
    }
}
