﻿// <copyright file="TetelLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Data;
    using Repository;

    /// <summary>
    /// Tétel logikáért felelős osztály
    /// </summary>
    public class TetelLogic : ParentLogic, ITetel
    {
        private ITetelRepo repo;

        /// <summary>
        /// Initializes a new instance of the <see cref="TetelLogic"/> class.
        /// paraméter nélküli konstruktora
        /// </summary>
        public TetelLogic()
        {
            this.repo = new TetelRepo();
        }

        /// <summary>
        /// Tétel törlése
        /// </summary>
        /// <param name="tetelID">kell hozzá a tétel idja</param>
        /// <returns>válasz</returns>
        public async Task<string> DeleteTetel(int tetelID)
        {
            try
            {
                await this.repo.Delete(tetelID);
                return "Sikeresen törölve";
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }

        /// <summary>
        /// Tételek megkapása
        /// </summary>
        /// <param name="tetelID">szükséges hozzá a tétel id-ja</param>
        /// <returns>Tétel vagy tételek visszakapása</returns>
        public async Task<List<Tetel>> GetTetel(int? tetelID = null)
        {
            try
            {
                if (tetelID == null)
                {
                    return (await this.repo.GetAll()).ToList();
                }

                return new List<Tetel>() { await this.repo.GetById(tetelID.Value) };
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }

        /// <summary>
        /// Tétel módosítása
        /// </summary>
        /// <param name="tetelID">szükkséges hozzá a tétel id-ja</param>
        /// <param name="cikkszam">cikkszám kell</param>
        /// <param name="mennyiseg">mennyisége</param>
        /// <returns>válasz (string)</returns>
        public async Task<string> ModifyTetel(int tetelID, int? cikkszam = null, int? mennyiseg = null)
        {
            if (new TermekLogic().GetTermek(cikkszam) == null)
            {
                throw new ArgumentException("Érvénytelen cikkszám");
            }

            try
            {
                await this.repo.ModifyTetel(tetelID, cikkszam: cikkszam, mennyiseg: mennyiseg);
                return "Sikeresen módosítva";
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Ismeretlen hiba");
            }
        }
    }
}
