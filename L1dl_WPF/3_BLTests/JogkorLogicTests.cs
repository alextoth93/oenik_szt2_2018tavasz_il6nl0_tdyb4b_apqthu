﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Tests
{
    [TestClass()]
    public class JogkorLogicTests
    {
        [TestMethod()]
        public void GetJogkorTest()
        {
            JogkorLogic logic = new JogkorLogic();
            var r = logic.GetJogkor().Result;
            Assert.IsNotNull(r);
        }

        [TestMethod()]
        public void GetJogkorTest_single()
        {
            JogkorLogic logic = new JogkorLogic();
            var r = logic.GetJogkor(1).Result;
            Assert.IsNotNull(r);
        }


    }
}