﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using L1dl_WPF.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data;
using L1dl_WPF.ViewModel;

namespace L1dl_WPF.View.Tests
{
    [TestClass()]
    public class HomeTests
    {
        [TestMethod()]
        public void HomeTest()
        {
            Felhasznalo f = new Felhasznalo() { FelhasznaloId = 66, FelhasznaloNev = "teszt", JogkorId = 42 };
            Home w = new Home(f);
            Assert.AreEqual(66, HomeVM.Aktualis.FelhasznaloId);
        }
    }
}