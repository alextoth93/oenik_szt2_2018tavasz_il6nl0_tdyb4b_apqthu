﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using L1dl_WPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1dl_WPF.ViewModel.Tests
{
    [TestClass()]
    public class TermekHozzaadasModositasVMTests
    {
        [TestMethod()]
        public void TermekHozzaadasModositasVMTest()
        {
            List<dynamic> list = new List<dynamic>();
            list.Add(new { Cikkszám = 10000, Terméknév = "Fagyi" });
            TermekHozzaadasModositasVM vm = new TermekHozzaadasModositasVM(list, "uj");


            Assert.AreEqual(false, vm.ModositisActive);
        }

    }
}