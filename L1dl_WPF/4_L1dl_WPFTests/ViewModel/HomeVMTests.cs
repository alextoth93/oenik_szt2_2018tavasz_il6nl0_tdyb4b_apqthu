﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using L1dl_WPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1dl_WPF.ViewModel.Tests
{
    [TestClass()]
    public class HomeVMTests
    {
        [TestMethod()]
        public void ListazasTest()
        {
            HomeVM vm = new HomeVM();
            vm.Beszallitok = new List<dynamic>();
            var old = vm.Beszallitok.Count();
            vm.Listazas().Wait();
            var newasd = vm.Beszallitok.Count();
            Assert.AreNotEqual(old, newasd);
        }
    }
}