﻿// <copyright file="BeszallitoHozzaadasModositasVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace L1dl_WPF.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Controls;
    using BL;
    using Data;

    /// <summary>
    /// BeszallitoHozzaadasModositásáért felelős viewmodell
    /// </summary>
    public class BeszallitoHozzaadasModositasVM : INotifyPropertyChanged
    {
        private HomeVM homeVM = new HomeVM();
        private TextBox beszallitoNevTextBox = new TextBox();
        private ComboBox beszallitoTermekeComboBox = new ComboBox();
        private Button termekHozzaadButton = new Button();
        private Button beszallitoHozzaadModositButton = new Button();
        private ListBox beszallitoTermekeiList = new ListBox();
        private List<dynamic> termekLista = new List<dynamic>();
        private List<dynamic> list = new List<dynamic>();
        private string route;
        private DataGrid dg = new DataGrid();
        private object selectedDataGridItem;
        private bool btnListabolKi = true;
        private ITermek termekLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="BeszallitoHozzaadasModositasVM"/> class.
        /// két paraméteres konstruktor
        /// </summary>
        /// <param name="list">dinamikus lista</param>
        /// <param name="eventRoute">eventroute(string)</param>
        public BeszallitoHozzaadasModositasVM(List<dynamic> list, string eventRoute)
        {
            this.termekLogic = new TermekLogic();
            Thread.Sleep(2000);
            var s = this.homeVM.Termekek;
            foreach (var item in s)
            {
                this.termekLista.Add(new { item.Terméknév, item.Cikkszám });
            }

            this.List = list;
            this.Route = eventRoute;
            if (this.Route == "modosit")
            {
                this.BeszallitoNevTextBox.Text = list.ElementAt(0).Beszállítónév;
                foreach (var item in list.ElementAt(0).Termékei)
                {
                    this.beszallitoTermekeiList.Items.Add(new { Terméknév= item.Termek.TermekNev, Cikkszám = item.Cikkszam });
                }
            }
            else
            {
                this.BeszallitoNevTextBox.Text = string.Empty;
                this.beszallitoTermekeiList.Items.Clear();
            }
        }

        /// <summary>
        /// Property megváltozása esetén felelős eventhandler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Beszállító Név textboxa
        /// </summary>
        public TextBox BeszallitoNevTextBox
        {
            get
            {
                return this.beszallitoNevTextBox;
            }

            set
            {
                this.beszallitoNevTextBox = value;
                this.OnPropertyChanged("BeszallitoNevTextBox");
            }
        }

        /// <summary>
        /// Termék hozzáadás buttonja
        /// </summary>
        public Button TermekHozzaadButton
        {
            get
            {
                return this.termekHozzaadButton;
            }

            set
            {
                this.termekHozzaadButton = value;
                this.OnPropertyChanged("TermekHozzaadButton");
            }
        }

        /// <summary>
        /// Beszállltó Hozzáadása/módosítása gombja
        /// </summary>
        public Button BeszallitoHozzaadModositButton
        {
            get
            {
                return this.beszallitoHozzaadModositButton;
            }

            set
            {
                this.beszallitoHozzaadModositButton = value;
                this.OnPropertyChanged("BeszallitoHozzaadModositButton");
            }
        }

        /// <summary>
        /// Beszállító termék listája
        /// </summary>
        public ListBox BeszallitoTermekeiList
        {
            get
            {
                return this.beszallitoTermekeiList;
            }

            set
            {
                this.beszallitoTermekeiList = value;
                this.OnPropertyChanged("BeszallitoTermekeiListView");
            }
        }

        /// <summary>
        /// Beszállító Termékének a comboboxa
        /// </summary>
        public ComboBox BeszallitoTermekeComboBox
        {
            get
            {
                return this.beszallitoTermekeComboBox;
            }

            set
            {
                this.beszallitoTermekeComboBox = value;
                this.OnPropertyChanged("BeszallitoTermekeComboBox");
            }
        }

        /// <summary>
        /// Termék lista (dinamikus)
        /// </summary>
        public List<dynamic> TermekLista
        {
            get
            {
                return this.termekLista;
            }

            set
            {
                this.termekLista = value;
                this.OnPropertyChanged("TermekLista");
            }
        }

        /// <summary>
        /// Route
        /// </summary>
        public string Route
        {
            get
            {
                return this.route;
            }

            set
            {
                this.route = value;
                this.OnPropertyChanged("Route");
            }
        }

        /// <summary>
        /// Datagrid
        /// </summary>
        public DataGrid Dg
        {
            get
            {
                return this.dg;
            }

            set
            {
                this.dg = value;
                this.OnPropertyChanged("Dg");
            }
        }

        /// <summary>
        /// Kiválasztott DataGrid elem
        /// </summary>
        public object SelectedDataGridItem
        {
            get
            {
                return this.selectedDataGridItem;
            }

            set
            {
                this.selectedDataGridItem = value;
                this.OnPropertyChanged("SelectedDataGridItem");
            }
        }

        /// <summary>
        /// Dinamikus lista
        /// </summary>
        public List<dynamic> List
        {
            get
            {
                return this.list;
            }

            set
            {
                this.list = value;
                this.OnPropertyChanged("List");
            }
        }

        /// <summary>
        /// Listából ki (bool)
        /// </summary>
        public bool BtnListabolKi
        {
            get
            {
                return this.btnListabolKi;
            }

            set
            {
                this.btnListabolKi = value;
                this.OnPropertyChanged("BtnListabolKi");
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
