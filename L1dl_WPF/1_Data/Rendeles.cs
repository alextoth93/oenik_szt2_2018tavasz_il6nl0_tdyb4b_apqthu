namespace Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Rendeles
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Rendeles()
        {
            Tetel = new HashSet<Tetel>();
        }

        public int RendelesId { get; set; }

        public int BeszallitoId { get; set; }

        [Column(TypeName = "date")]
        public DateTime Datum { get; set; }

        public int FelelosSzemelyId { get; set; }

        public virtual Beszallito Beszallito { get; set; }

        public virtual Felhasznalo Felhasznalo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Tetel> Tetel { get; set; }
    }
}
