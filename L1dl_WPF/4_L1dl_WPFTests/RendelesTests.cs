﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using L1dl_WPF.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L1dl_WPF.View.Tests
{
    [TestClass()]
    public class RendelesTests
    {
        [TestMethod()]
        public void TiltoTest()
        {
            Rendeles w = new Rendeles(new ViewModel.RendelesVM(null, "uj"));
            var old = w.TesztBox.IsEnabled;
            w.Tilto();
            var newasd = w.TesztBox.IsEnabled;
            Assert.AreNotEqual(old, newasd);
        }

        [TestMethod()]
        public void EngedelyezoTest()
        {
            Rendeles w = new Rendeles(new ViewModel.RendelesVM(null, "uj"));
            w.TesztBox.IsEnabled = false; 
            var old = w.TesztBox.IsEnabled;
            w.Engedelyezo();
            var newasd = w.TesztBox.IsEnabled;
            Assert.AreNotEqual(old, newasd);
        }
    }
}