﻿// <copyright file="ITermekRepo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    public interface ITermekRepo : IGenericRepository<Data.Termek>
    {
        Task<bool> ErvenyesE(int cikkszam);

        Task ModifyTermek(int cikkszam, int? vonalkod = null, string nev = null, string leiras = null, int? ar = null, int? mennyiseg = null, string toroltE = null);
    }
}
